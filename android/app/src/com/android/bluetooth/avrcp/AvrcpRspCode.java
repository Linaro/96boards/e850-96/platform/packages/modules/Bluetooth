/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.bluetooth.avrcp;

public class AvrcpRspCode {
    /* Do not modify without upating Status in avrcp_common.h*/
    // Response Error codes
    public static final byte INVALID_COMMAND = 0x00; /* Invalid command */
    public static final byte INVALID_PARAMETER = 0x01; /* Invalid parameter */
    public static final byte PARAMETER_CONTENT_ERROR = 0x02; /* Specified parameter is wrong or not found */
    public static final byte INTERNAL_ERROR = 0x03; /* Internal Error */
    public static final byte NO_ERROR = 0x04; /* Operation Success */
    public static final byte UIDS_CHANGED = 0x05; /* UIDs changed */
    public static final byte RESERVED = 0x06; /* Reserved */
    public static final byte INVALID_DIRECTION = 0x07; /* Invalid direction */
    public static final byte NOT_A_DIRECTORY = 0x08; /* Invalid directory */
    public static final byte DOES_NOT_EXIST = 0x09; /* Invalid Item */
    public static final byte INVALID_SCOPE = 0x0a; /* Invalid scope */
    public static final byte RANGE_OUT_OF_BOUNDS = 0x0b; /* Invalid range */
    public static final byte FOLDER_ITEM_NOT_PLAYABLE = 0x0c; /* UID is a directory */
    public static final byte MEDIA_IN_USE = 0x0d; /* Media in use */
    public static final byte NOW_PLAYING_LIST_FULL = 0x0e; /* Playing list full */
    public static final byte SEARCH_NOT_SUPPORTED = 0x0f; /* Search not supported */
    public static final byte SEARCH_IN_PROGRESS = 0x10; /* Search in progress */
    public static final byte INVALID_PLAYER_ID = 0x11; /* Invalid player */
    public static final byte PLAYER_NOT_BROWSABLE = 0x12; /* Player not browsable */
    public static final byte PLAYER_NOT_ADDRESSED = 0x13; /* Player not addressed */
    public static final byte NO_VALID_SEARCH_RESULTS = 0x14; /* Invalid results */
    public static final byte NO_AVAILABLE_PLAYERS = 0x15; /* No available players */
    public static final byte ADDRESSED_PLAYER_CHANGED = 0x16; /* Addressed player changed */
}
