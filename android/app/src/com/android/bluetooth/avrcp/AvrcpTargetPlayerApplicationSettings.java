/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.bluetooth.avrcp;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.util.Pair;

import com.android.bluetooth.util.LogUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class AvrcpTargetPlayerApplicationSettings {
    private static final String TAG = "AvrcpTargetPlayerApplicationSettings";

    private Context mContext;
    private AvrcpNativeInterface mNativeInterface;
    private ArrayList<Integer> mPendingCmds;
    private ArrayList<Integer> mPendingSetAttributes;
    private ArrayList<Byte> mPlayerSettingCmds;
    private PlayerApplicationSettingsRspInterface mPlayerApplicationSettingsRspInterface;

    public interface PlayerApplicationSettingsRspInterface {
        public boolean updatePendingCmdResponse(int reponse);
    }

    private class PlayerSettings {
        public byte[] attrs;
    };
    private PlayerSettings mPlayerSettings = new PlayerSettings();

    private class localSettingValue {
        public byte value;
        public boolean isSelected;
    }
    private Map<Byte, List<localSettingValue>> mLocalSettings = new HashMap<>();

    private static final String BLUETOOTH_ADMIN_PERM = android.Manifest.permission.BLUETOOTH_ADMIN;
    private static final String BLUETOOTH_PERM = android.Manifest.permission.BLUETOOTH;

    private static final String COMMAND = "command";
    private static final String CMDGET = "get";
    private static final String CMDSET = "set";
    private static final String EXTRA_GET_COMMAND = "commandExtra";
    private static final String EXTRA_GET_RESPONSE = "Response";

    private static final String EXTRA_ATTRIBUTE_ID = "Attribute";
    private static final String EXTRA_VALUE_STRING_ARRAY = "ValueStrings";
    private static final String EXTRA_ATTRIB_VALUE_PAIRS = "AttribValuePairs";
    private static final String EXTRA_ATTRIBUTE_STRING_ARRAY = "AttributeStrings";
    private static final String EXTRA_VALUE_ID_ARRAY = "Values";
    private static final String EXTRA_ATTIBUTE_ID_ARRAY = "Attributes";

    public static final byte VALUE_SHUFFLEMODE_OFF = 1;
    public static final byte VALUE_SHUFFLEMODE_ALL = 2;
    public static final byte VALUE_REPEATMODE_OFF = 1;
    public static final byte VALUE_REPEATMODE_SINGLE = 2;
    public static final byte VALUE_REPEATMODE_ALL = 3;
    public static final byte ATTRIBUTE_NOTSUPPORTED = -1;

    public static final byte ATTRIBUTE_EQUALIZER = 1;
    public static final byte ATTRIBUTE_REPEATMODE = 2;
    public static final byte ATTRIBUTE_SHUFFLEMODE = 3;
    public static final byte ATTRIBUTE_SCANMODE = 4;

    public static final int GET_ATTRIBUTE_IDS = 0;
    public static final int GET_VALUE_IDS = 1;
    public static final int GET_ATTRIBUTE_TEXT = 2;
    public static final int GET_VALUE_TEXT = 3;
    public static final int GET_ATTRIBUTE_VALUES = 4;
    public static final int NOTIFY_ATTRIBUTE_VALUES = 5;
    public static final int SET_ATTRIBUTE_VALUES  = 6;
    public static final int GET_INVALID = 0xff;

    //Values of default (timeout)
    private static final byte[] DEFAULT_ATTRIBS = new byte[] {
            ATTRIBUTE_REPEATMODE,
            ATTRIBUTE_SHUFFLEMODE,
    };
    private static final byte[] REPEATMODE_VALUES = new byte[] {
            VALUE_REPEATMODE_OFF,
            VALUE_REPEATMODE_SINGLE,
            VALUE_REPEATMODE_ALL,
    };
    private static final byte[] SHUFFLEMODE_VALUES = new byte[] {
            VALUE_SHUFFLEMODE_OFF,
            VALUE_SHUFFLEMODE_ALL
    };
    private static final byte[] EMPTY_ARR = {};
    private static final String[] EMPTY_STR_ARR = {};
    private static final byte[] EMPTY_VALUE = new byte[] {
            0
    };

    private static final String PLAYERSETTINGS_REQUEST = "com.samsung.slsi.music.playersettings.request";
    private static final String PLAYERSETTINGS_RESPONSE = "com.samsung.slsi.music.playersettings.response";

    //Intent name to work with BBK music (iMusic)
    //private static final String PLAYERSETTINGS_REQUEST = "org.codeaurora.music.playersettingsrequest";
    //private static final String PLAYERSETTINGS_RESPONSE =  "org.codeaurora.music.playersettingsresponse";

    public AvrcpTargetPlayerApplicationSettings(Context context,
                                                AvrcpNativeInterface nativeInterface,
                                                PlayerApplicationSettingsRspInterface pasRsp) {
        mContext = context;
        mNativeInterface = nativeInterface;
        mPendingCmds = null;
        mPendingSetAttributes = null;
        mPlayerApplicationSettingsRspInterface = pasRsp;
    }

    public void start() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PLAYERSETTINGS_RESPONSE);
        mContext.registerReceiver(mIntentReceiver, intentFilter);
        mPendingCmds = new ArrayList<>();
        mPendingSetAttributes = new ArrayList<>();
        mPlayerSettingCmds = new ArrayList<>();
    }

    public void cleanup() {
        mLocalSettings.clear();
        mContext.unregisterReceiver(mIntentReceiver);
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logD("onReceive(): " + intent);
            String action = intent.getAction();
            if (action.equals(PLAYERSETTINGS_RESPONSE)) {
                int rspCode = intent.getIntExtra(EXTRA_GET_RESPONSE, GET_INVALID);
                logV("rspCode = " + rspCodeToString(rspCode));
                byte[] data;
                String[] text;
                boolean isSetAttrValRsp = false;

                synchronized (mPendingCmds) {
                      if (mPendingCmds.contains(new Integer(rspCode))) {
                        if (rspCode == SET_ATTRIBUTE_VALUES) {
                            isSetAttrValRsp = true;
                            logV("SET_ATTRIBUTE_VALUES response!");
                        }
                        mPendingCmds.remove(new Integer(rspCode));
                    }
                }
                mPlayerApplicationSettingsRspInterface.updatePendingCmdResponse(rspCode);
                Pair<Byte, byte[]> set;
                switch (rspCode) {
                    case GET_ATTRIBUTE_IDS:
                        data = intent.getByteArrayExtra(EXTRA_ATTIBUTE_ID_ARRAY);
                        logV("data = " + byteArrayToHex(data) + " length: " + data.length);
                        addLocalPlayerSettingIds(data);

                        set = retrieveLocalPlayerSettingIds();
                        mNativeInterface.getListPlayerApplicationAttrRsp(set.first, set.second);
                        break;
                    case GET_VALUE_IDS:
                        byte attrib = 0;
                        if (!mPlayerSettingCmds.isEmpty()) {
                            attrib = mPlayerSettingCmds.get(0);
                            mPlayerSettingCmds.remove(0);
                        } else {
                            Log.w(TAG, "No request commands in QUEUE!");
                            break;
                        }
                        data = intent.getByteArrayExtra(EXTRA_VALUE_ID_ARRAY);
                        logV("data = " + byteArrayToHex(data) + " length: " + data.length);
                        addLocalPlayerSettingValues(attrib, data);
                        set = retrieveLocalPlayerSettingValues(attrib);
                        if (set != null) {
                            mNativeInterface.getPlayerApplicationValueRsp(set.first,
                                    set.second, AvrcpRspCode.NO_ERROR);
                        } else {
                            mNativeInterface.getPlayerApplicationValueRsp(
                                    0,
                                    EMPTY_ARR,
                                    AvrcpRspCode.PARAMETER_CONTENT_ERROR);
                        }
                        break;
                    case GET_ATTRIBUTE_VALUES:
                        data = intent.getByteArrayExtra(EXTRA_ATTRIB_VALUE_PAIRS);
                        logV("data = " + byteArrayToHex(data) + " length: " + data.length);
                        updateLocalPlayerSettings(data);
                        set = retrieveLocalPlayerSettings();
                        mNativeInterface.sendCurrentPlayerValueRsp(set.first, set.second,
                                AvrcpRspCode.NO_ERROR);
                        break;
                    case SET_ATTRIBUTE_VALUES:
                        boolean send_change_rsp_only = true;
                        data = intent.getByteArrayExtra(EXTRA_ATTRIB_VALUE_PAIRS);
                        updateLocalPlayerSettings(data);
                        if (isSetAttrValRsp) {
                            isSetAttrValRsp = false;
                            send_change_rsp_only = false;
                            boolean success = checkPlayerAttributeResponse(data);
                            sendPlayerAppChangedRsp(
                                    success ? AvrcpRspCode.NO_ERROR : AvrcpRspCode.INTERNAL_ERROR,
                                    true);
                            mPendingSetAttributes.clear();
                        } else {
                            sendPlayerAppChangedRsp(AvrcpRspCode.NO_ERROR, false);
                        }
                        break;
                    case GET_ATTRIBUTE_TEXT:
                        text = intent.getStringArrayExtra(EXTRA_ATTRIBUTE_STRING_ARRAY);
                        logV("attrIds count: " + mPlayerSettings.attrs.length);
                        for (int i = 0; i < mPlayerSettings.attrs.length; ++i) {
                            logV("attr string[" + i + "] = " + text[i]);
                        }

                        mNativeInterface.sendPlayerApplicationSettingsTextRsp(
                                mPlayerSettings.attrs.length,
                                mPlayerSettings.attrs,
                                text.length,
                                text,
                                AvrcpRspCode.NO_ERROR);
                        break;
                    case GET_VALUE_TEXT:
                        text = intent.getStringArrayExtra(EXTRA_VALUE_STRING_ARRAY);
                        logV("attrIds count: " + mPlayerSettings.attrs.length);
                        for (int i = 0; i < mPlayerSettings.attrs.length; ++i) {
                            logV("value string[" + i + "] = " + text[i]);
                        }

                        mNativeInterface.sendPlayerApplicationValueTextRsp(
                                mPlayerSettings.attrs.length,
                                mPlayerSettings.attrs,
                                text.length,
                                text,
                                AvrcpRspCode.NO_ERROR);
                        break;
                }
            }
        }
    };

    public void handlerMsgTimeout(int command) {
        logV("handlerMsgTimeout");
        synchronized (mPendingCmds) {
            if (!mPendingCmds.contains(new Integer(command))) {
                return;
            }
            mPendingCmds.remove(new Integer(command));
        }
        Pair<Byte, byte[]> set;
        switch (command) {
            case GET_ATTRIBUTE_IDS:
                addLocalPlayerSettingIds(DEFAULT_ATTRIBS);
                set = retrieveLocalPlayerSettingIds();
                mNativeInterface.getListPlayerApplicationAttrRsp(set.first, set.second);
                break;
            case GET_VALUE_IDS:
                byte attrib = 0;
                if (!mPlayerSettingCmds.isEmpty()) {
                    attrib = mPlayerSettingCmds.get(0);
                    mPlayerSettingCmds.remove(0);
                } else {
                    Log.w(TAG, "No request commands in QUEUE!");
                    break;
                }
                switch (attrib) {
                    case ATTRIBUTE_REPEATMODE:
                        addLocalPlayerSettingValues(attrib, REPEATMODE_VALUES);
                        set = retrieveLocalPlayerSettingValues(attrib);
                        if (set != null) {
                            mNativeInterface.getPlayerApplicationValueRsp(
                                    set.first, set.second, AvrcpRspCode.NO_ERROR);
                        } else {
                            mNativeInterface.getPlayerApplicationValueRsp(
                                    0,
                                    EMPTY_ARR,
                                    AvrcpRspCode.PARAMETER_CONTENT_ERROR);
                        }
                        break;
                    case ATTRIBUTE_SHUFFLEMODE:
                        addLocalPlayerSettingValues(attrib, REPEATMODE_VALUES);
                        set = retrieveLocalPlayerSettingValues(attrib);
                        if (set != null) {
                            mNativeInterface.getPlayerApplicationValueRsp(
                                    set.first, set.second, AvrcpRspCode.NO_ERROR);
                        } else {
                            mNativeInterface.getPlayerApplicationValueRsp(
                                    0,
                                    EMPTY_ARR,
                                    AvrcpRspCode.PARAMETER_CONTENT_ERROR);
                        }
                        break;
                    default:
                        Log.w(TAG, "default: " + attrib + " check right value set");
                        mNativeInterface.getPlayerApplicationValueRsp(
                                EMPTY_VALUE.length,
                                EMPTY_VALUE,
                                AvrcpRspCode.NO_ERROR);
                        break;
                }
                break;
            case GET_ATTRIBUTE_VALUES:
                set = retrieveLocalPlayerSettings();
                mNativeInterface.sendCurrentPlayerValueRsp(set.first, set.second,
                        AvrcpRspCode.NO_ERROR);
                break;
            case SET_ATTRIBUTE_VALUES :
                mNativeInterface.sendSetPlayerApplicationRsp(AvrcpRspCode.NO_ERROR);
                break;
            case GET_ATTRIBUTE_TEXT:
                String[] attribText = new String[mPlayerSettings.attrs.length];
                for (int i = 0; i < mPlayerSettings.attrs.length; i++) {
                    attribText[i] = "";
                }
                mNativeInterface.sendPlayerApplicationSettingsTextRsp(
                        mPlayerSettings.attrs.length,
                        mPlayerSettings.attrs,
                        attribText.length,
                        attribText,
                        AvrcpRspCode.NO_ERROR);
                break;
            case GET_VALUE_TEXT:
                String[] valueText = new String[mPlayerSettings.attrs.length];
                for (int i = 0; i < mPlayerSettings.attrs.length; i++) {
                    valueText[i] = "";
                }
                mNativeInterface.sendPlayerApplicationValueTextRsp(
                        mPlayerSettings.attrs.length,
                        mPlayerSettings.attrs,
                        valueText.length,
                        valueText,
                        AvrcpRspCode.NO_ERROR);
                break;
            default :
                Log.e(TAG, "UNKNOWN command");
                break;
        }
    }

    private void addLocalPlayerSettingIds(byte[] attrIds) {
        for (int i = 0; i < attrIds.length; i++) {
            if (mLocalSettings.containsKey(new Byte(attrIds[i]))) {
                Log.i(TAG, "attrId: " + attrIdToString(attrIds[i]) + " is exist!");
            } else {
                mLocalSettings.put(new Byte(attrIds[i]), new ArrayList<localSettingValue>());
            }
        }
    }

    private void addLocalPlayerSettingValues(byte attrId, byte[] attrValues) {
        if (mLocalSettings.containsKey(new Byte(attrId))) {
            boolean needReset = false;
            boolean isFound = false;
            List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrId));
            for (int i = 0; i < attrValues.length; i++) {
                isFound = false;
                for (localSettingValue lsv : settingValues) {
                    if (lsv.value == attrValues[i]) {
                        isFound = true;
                        Log.w(TAG, "attrValue: " + attrValueToString(attrId, attrValues[i])
                                + " is not stored in local settings for " + attrIdToString(attrId)
                                + " and isSelected = " + lsv.isSelected);
                        break;
                    }
                }
                if (!isFound) {
                    needReset = true;
                    Log.w(TAG, "attrValue: " + attrValueToString(attrId, attrValues[i])
                            + " is not stored in local settings for " + attrIdToString(attrId)
                            + ", so we reset this settings");
                    break;
                }
            }

            if (needReset) {
                settingValues.clear();
                for (int i = 0; i < attrValues.length; i++) {
                    localSettingValue settingValue = new localSettingValue();
                    settingValue.value = attrValues[i];
                    settingValue.isSelected = false;
                    settingValues.add(settingValue);
                }
            }
        } else {
            Log.w(TAG, "attrId: " + attrIdToString(attrId) + " is not stored in local settings!");
        }
    }

    private Pair<Byte, byte[]> retrieveLocalPlayerSettingIds() {
        byte[] attrIds = new byte[mLocalSettings.size()];
        byte size = 0;
        for (byte key: mLocalSettings.keySet()) {
            attrIds[size++] = key;
        }
        logV("retrieveLocalPlayerSettingIds: (hex) attrIds = " + byteArrayToHex(attrIds));

        return new Pair<Byte, byte[]>(new Byte(size), attrIds);
    }

    private Pair<Byte, byte[]> retrieveLocalPlayerSettingValues(byte attrId) {
        if (!mLocalSettings.containsKey(new Byte(attrId))) {
            Log.e(TAG, "attrId: " + attrIdToString(attrId) + " is not stored in local settings!");
            return null;
        }
        List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrId));
        byte[] attrValues = new byte[settingValues.size()];
        byte size = 0;
        for (localSettingValue lsv : settingValues) {
            logD("Adding attrValue: " + attrValueToString(attrId, lsv.value)
                    + " into player setting values for attrId: " + attrId);
            attrValues[size++] = lsv.value;
        }
        logV("retrieveLocalPlayerSettingValues: (hex) attrValues = " + byteArrayToHex(attrValues));

        return new Pair<Byte, byte[]>(new Byte(size), attrValues);
    }

    private Pair<Byte, byte[]> retrieveLocalPlayerSettings() {
        byte[] retVal = new byte[mLocalSettings.size() * 2];
        byte size = 0;
        for (byte attrId : mLocalSettings.keySet()) {
            retVal[size++] = attrId;
            List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrId));
            boolean isFound = false;
            if (settingValues.size() == 0) {
                //TODO: check validity value, list size 0 means no GET VALUES done prior.
                //In case of this, we add default value OFF(0x01) to list (REPEAT / SHUFFLE)
                if (attrId == ATTRIBUTE_REPEATMODE) {
                    addLocalPlayerSettingValues(attrId, REPEATMODE_VALUES);
                } else if (attrId == ATTRIBUTE_SHUFFLEMODE) {
                    addLocalPlayerSettingValues(attrId, SHUFFLEMODE_VALUES);
                }
                settingValues = mLocalSettings.get(new Byte(attrId));
                retVal[size++] = settingValues.get(0).value;
                settingValues.get(0).isSelected = true;
            } else {
                for (localSettingValue lsv : settingValues) {
                    if (lsv.isSelected) {
                        retVal[size++] = lsv.value;
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    Log.w(TAG, "No values are selected from attrId: " + attrIdToString(attrId));
                    retVal[size++] = settingValues.get(0).value;
                    settingValues.get(0).isSelected = true;
                }
            }
        }
        logV("retrieveLocalPlayerSettings: (hex) retVal = " + byteArrayToHex(retVal));

        return new Pair<Byte, byte[]>(new Byte(size), retVal);
    }

    private byte checkLocalPlayerSettingAttributesValid(byte num, byte[] attrIds, byte[] attrValues) {
        if (num <= 0) {
            Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attr num is 0");
            return AvrcpRspCode.INVALID_PARAMETER;
        }

        for (int i = 0; i < num; i++) {
            if (!mLocalSettings.containsKey(new Byte(attrIds[i]))) {
                if (!isValidAttributeId(attrIds[i])) {
                    Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attrId: "
                            + attrIdToString(attrIds[i])
                            + " is not understandable, not found in local settings");
                    return AvrcpRspCode.INVALID_PARAMETER;
                } else {
                    Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attrId: "
                            + attrIdToString(attrIds[i])
                            + " is not in local settings");
                    continue;
                }
            }

            if (attrValues != null) {
                boolean isFound = false;
                List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrIds[i]));
                for (localSettingValue lsv : settingValues) {
                    if (lsv.value == attrValues[i]) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attrValue: "
                            + attrValueToString(attrIds[i], attrValues[i])
                            + " is not selectable from local player settting of "
                            + attrIdToString(attrIds[i]));
                    return AvrcpRspCode.PARAMETER_CONTENT_ERROR;
                }
            }
        }

        return AvrcpRspCode.NO_ERROR;
    }

    private byte checkLocalPlayerSettingAttributesValid(byte attrId, byte valueNum,
                                                        byte[] attrValues) {
        if (!mLocalSettings.containsKey(new Byte(attrId))) {
            Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attrId: "
                    + attrIdToString(attrId)
                    + " is not understandable, not found in local settings");
            return AvrcpRspCode.INVALID_PARAMETER;
        }

        List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrId));
        if (valueNum <= 0 || valueNum > settingValues.size()) {
            Log.w(TAG, "checkLocalPlayerSettingAttributesValid: attrValue num is not valid");
            return AvrcpRspCode.INVALID_PARAMETER;
        }

        for (int i = 0; i < attrValues.length; i++) {
            boolean isExist = false;
            for (localSettingValue lsv : settingValues) {
                if (attrValues[i] == lsv.value) {
                    logD("checkLocalPlayerSettingAttributesValid: attrValue: "
                            + attrValueToString(attrId, attrValues[i])
                            + " exist in local settings");
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                Log.w(TAG, "checkLocalPlayerSettingAttributesValid: some value is not " +
                        "include in local setting values for attrId " + attrIdToString(attrId));
                return AvrcpRspCode.INVALID_PARAMETER;
            }
        }

        return AvrcpRspCode.NO_ERROR;
    }

    private void updateLocalPlayerSettings(byte[] data) {
        for (int i = 0; i < data.length; i += 2) {
            logV("updateLocalPlayerSettings: attrId: " + attrIdToString(data[i])
                    + " attrValue: " + attrValueToString(data[i], data[i+1]));
            List<localSettingValue> settingValues = mLocalSettings.get(new Byte(data[i]));
            if (settingValues == null) {
                Log.w(TAG, "updateLocalPlayerSettings: Local settings for attrId: "
                        + attrIdToString(data[i]) + " is not found!");
                continue;
            }
            for (localSettingValue lsv : settingValues) {
                lsv.isSelected = (lsv.value == data[i+1]);
            }
        }
    }

    private boolean checkPlayerAttributeResponse(byte[] data) {
        boolean ret = false;
        for (int i = 0; i < data.length; i += 2) {
            logV("checkPlayerAttributeResponse: attrId: " + attrIdToString(data[i])
                    + " attrValue: " + attrValueToString(data[i], data[i+1]));
            if (mPendingSetAttributes.contains(new Integer(data[i]))) {
                logV("checkPlayerAttributeResponse: Pending SetAttribute contains " +
                        attrIdToString(data[i]));
                ret = (data[i+1] != ATTRIBUTE_NOTSUPPORTED);
            }
        }
        return ret;
    }

    public void listPlayerAttributeIdsRequest() {
        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDGET);
        intent.putExtra(EXTRA_GET_COMMAND, GET_ATTRIBUTE_IDS);
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(GET_ATTRIBUTE_IDS));
    }

    public boolean getPlayerValueIdsRequest(byte attrId) {
        if (!mLocalSettings.containsKey(new Byte(attrId))) {
            Log.w(TAG, "getPlayerValueIdsRequest: " + attrIdToString(attrId)
                    + " is not stored in local settings");
            mNativeInterface.getPlayerApplicationValueRsp(
                    0,
                    EMPTY_ARR,
                    AvrcpRspCode.INVALID_PARAMETER);
            return false;
        }

        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDGET);
        intent.putExtra(EXTRA_GET_COMMAND, GET_VALUE_IDS);
        intent.putExtra(EXTRA_ATTRIBUTE_ID, attrId);
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(GET_VALUE_IDS));
        mPlayerSettingCmds.add(new Byte(attrId));
        return true;
    }

    public boolean getCurrentPlayerValuesRequest(byte num, byte[] attrIds) {
        byte result = checkLocalPlayerSettingAttributesValid(num, attrIds, null);
        if (result != AvrcpRspCode.NO_ERROR) {
            mNativeInterface.sendCurrentPlayerValueRsp(
                    0,
                    EMPTY_ARR,
                    result);
            return false;
        }
        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDGET);
        intent.putExtra(EXTRA_GET_COMMAND, GET_ATTRIBUTE_VALUES);

        byte[] bArray = new byte[num];
        for (int j = 0; j < num ; j++) {
            bArray[j] = attrIds[j];
        }
        intent.putExtra(EXTRA_ATTIBUTE_ID_ARRAY, bArray);
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(GET_ATTRIBUTE_VALUES));
        return true;
    }

    public boolean setPlayerAttributeSettings(byte num, byte[] attrIds, byte[] attrVals) {
        byte result = checkLocalPlayerSettingAttributesValid(num, attrIds, attrVals);
        if (result != AvrcpRspCode.NO_ERROR) {
            mNativeInterface.sendSetPlayerApplicationRsp(result);
            return false;
        }
        byte[] array = new byte[num * 2];
        for (int i = 0; i < num; i++) {
            array[i] = attrIds[i] ;
            array[i+1] = attrVals[i];
            mPendingSetAttributes.add(new Integer(attrIds[i]));
        }
        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDSET);
        intent.putExtra(EXTRA_ATTRIB_VALUE_PAIRS, array);
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(SET_ATTRIBUTE_VALUES));
        return true;
    }

    public boolean getPlayerApplicationSettingsText(byte num , byte[] attrIds) {
        byte result = checkLocalPlayerSettingAttributesValid(num, attrIds, null);
        if (result != AvrcpRspCode.NO_ERROR) {
            mNativeInterface.sendPlayerApplicationSettingsTextRsp(
                    0,
                    EMPTY_ARR,
                    0,
                    EMPTY_STR_ARR,
                    result);
            return false;
        }
        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDGET);
        intent.putExtra(EXTRA_GET_COMMAND, GET_ATTRIBUTE_TEXT);
        intent.putExtra(EXTRA_ATTIBUTE_ID_ARRAY, attrIds);
        mPlayerSettings.attrs = new byte[num];
        for (int i = 0; i < num; i++) {
            mPlayerSettings.attrs[i] = attrIds[i];
        }
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(GET_ATTRIBUTE_TEXT));
        return true;
    }

    public boolean getPlayerApplicationValueText(byte attrId, byte num, byte[] attrVals) {
        byte result = checkLocalPlayerSettingAttributesValid(attrId, num, attrVals);
        if (result != AvrcpRspCode.NO_ERROR) {
            mNativeInterface.sendPlayerApplicationValueTextRsp(
                    0,
                    EMPTY_ARR,
                    0,
                    EMPTY_STR_ARR,
                    result);
            return false;
        }
        Intent intent = new Intent(PLAYERSETTINGS_REQUEST);
        intent.putExtra(COMMAND, CMDGET);
        intent.putExtra(EXTRA_GET_COMMAND, GET_VALUE_TEXT);
        intent.putExtra(EXTRA_ATTRIBUTE_ID, attrId);
        intent.putExtra(EXTRA_VALUE_ID_ARRAY, attrVals);
        mPlayerSettings.attrs = new byte[num];
        for (int i = 0; i < num; i++) {
            mPlayerSettings.attrs[i] = attrVals[i];
        }
        mContext.sendBroadcast(intent, BLUETOOTH_PERM);
        mPendingCmds.add(new Integer(GET_VALUE_TEXT));
        return true;
    }

    void sendPlayerAppChangedRsp(byte status, boolean sendRsp) {
        if (status == AvrcpRspCode.NO_ERROR) {
            logD("Player Application Settings changed, send notification");
            mNativeInterface.sendPlayerApplicationSettingsUpdate();
        }
        if (sendRsp) {
            logD("Send response for setPlayerAttributeSettings: " + status);
            mNativeInterface.sendSetPlayerApplicationRsp(status);
        }
    }

    public List<PlayerApplicationSettingAttribute> getLocalPlayerApplicationSettings() {
        List<PlayerApplicationSettingAttribute> pasAttrList =
                new ArrayList<PlayerApplicationSettingAttribute>();
        if (mLocalSettings.isEmpty()) {
            addLocalPlayerSettingIds(DEFAULT_ATTRIBS);
            addLocalPlayerSettingValues(ATTRIBUTE_REPEATMODE, REPEATMODE_VALUES);
            addLocalPlayerSettingValues(ATTRIBUTE_SHUFFLEMODE, SHUFFLEMODE_VALUES);
        }
        for (byte attrId : mLocalSettings.keySet()) {
            PlayerApplicationSettingAttribute playerApplicationSetting =
                    new PlayerApplicationSettingAttribute();
            List<localSettingValue> settingValues = mLocalSettings.get(new Byte(attrId));
            playerApplicationSetting.attrId = attrId;
            logV("local player setting attribute: " + attrIdToString(attrId));
            boolean isFound = false;
            for (localSettingValue lsv : settingValues) {
                if (lsv.isSelected) {
                    playerApplicationSetting.attrValue = lsv.value;
                    logV("local player setting value: " + attrValueToString(attrId, lsv.value));
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                Log.i(TAG, "No value found! add default value!!");
                playerApplicationSetting.attrValue = 0x01;      //OFF
                settingValues.get(0).isSelected = true;
            }
            pasAttrList.add(playerApplicationSetting);
        }
        return pasAttrList;
    }

    private static String rspCodeToString(int state) {
        switch (state) {
            case GET_ATTRIBUTE_IDS:
                return "GET_ATTRIBUTE_IDS";
            case GET_VALUE_IDS:
                return "GET_VALUE_IDS";
            case GET_ATTRIBUTE_TEXT:
                return "GET_ATTRIBUTE_TEXT";
            case GET_VALUE_TEXT:
                return "GET_VALUE_TEXT";
            case GET_ATTRIBUTE_VALUES:
                return "GET_ATTRIBUTE_VALUES";
            case NOTIFY_ATTRIBUTE_VALUES:
                return "NOTIFY_ATTRIBUTE_VALUES";
            case SET_ATTRIBUTE_VALUES:
                return "SET_ATTRIBUTE_VALUES";
            case GET_INVALID:
                return "GET_INVALID ";
            default:
                break;
        }
        return Integer.toString(state);
    }

    private static String attrIdToString(byte attrId) {
        switch (attrId) {
            case ATTRIBUTE_EQUALIZER:
                return "EQUALIZER";
            case ATTRIBUTE_REPEATMODE:
                return "REPEAT MODE";
            case ATTRIBUTE_SHUFFLEMODE:
                return "SHUFFLE MODE";
            case ATTRIBUTE_SCANMODE:
                return "SCAN MODE";
            default:
                break;
        }
        return "User Setting Id: " + byteArrayToHex(new byte[]{attrId}) ;
    }

    private static String attrValueToString(int attrId, byte attrValue) {
        switch (attrId) {
            case ATTRIBUTE_EQUALIZER: {
                switch (attrValue) {
                    case 0x01:
                        return "OFF";
                    case 0x02:
                        return "ON";
                    default:
                        return "Out of Range (0x" + byteArrayToHex(new byte[]{attrValue}) + ")";
                }
            }
            case ATTRIBUTE_REPEATMODE: {
                switch (attrValue) {
                    case VALUE_REPEATMODE_OFF:
                        return "OFF";
                    case VALUE_REPEATMODE_SINGLE:
                        return "SINGLE REPEAT";
                    case VALUE_REPEATMODE_ALL:
                        return "ALL TRACKS REPEAT";
                    default:
                        return "Out of Range (0x" + byteArrayToHex(new byte[]{attrValue}) + ")";
                }
            }
            case ATTRIBUTE_SHUFFLEMODE: {
                switch (attrValue) {
                    case VALUE_SHUFFLEMODE_OFF:
                        return "OFF";
                    case VALUE_SHUFFLEMODE_ALL:
                        return "ALL TRACKS SHUFFLE";
                    default:
                        return "Out of Range (0x" + byteArrayToHex(new byte[]{attrValue}) + ")";
                }
            }
            default:
        }
        return "User Setting Value: " + byteArrayToHex(new byte[]{attrValue});
    }

    private boolean isValidAttributeId(byte attrId) {
        return attrId >= ATTRIBUTE_EQUALIZER && attrId <= ATTRIBUTE_SCANMODE;
    }

    private static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder();
        for(final byte b: a) {
            sb.append(String.format("%02x ", b & 0xff));
        }
        return sb.toString();
    }

    private static void logD(String message) {
        LogUtil.logD(TAG, message);
    }

    private static void logV(String message) {
        LogUtil.logV(TAG, message);
    }
}
