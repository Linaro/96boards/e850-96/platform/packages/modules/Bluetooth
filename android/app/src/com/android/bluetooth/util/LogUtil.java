/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.bluetooth.util;

import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;

public final class LogUtil {
    public static final String TAG = "BluetoothLogUtil";

    private static final String BLUETOOTH_ENABLE_VERBOSE_LOGGING_PROPERTY =
            "persist.bluetooth.enable_verbose_logging";
    private static int MAX_BLUETOOTH_LOG_LEVEL;

    private LogUtil() {
        MAX_BLUETOOTH_LOG_LEVEL = SystemProperties.getBoolean(
                BLUETOOTH_ENABLE_VERBOSE_LOGGING_PROPERTY, false) ? Log.VERBOSE : Log.DEBUG;
    }

    /**
     * DEBUG logging with message
     * @param tag
     * @param message
     */
    public static void logD(String tag, String message) {
        if (isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, message);
        }
    }

    /**
     * DEBUG logging with message and params
     * @param tag
     * @param message
     * @param params
     */
    public static void logD(String tag, String message, Object... params) {
        if (isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, String.format(message, params));
        }
    }

    /**
     * VERBOSE logging with message
     * @param tag
     * @param message
     */
    public static void logV(String tag, String message) {
        if (isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, message);
        }
    }

    /**
     * VERBOSE logging with message and params
     * @param tag
     * @param message
     * @param params
     */
    public static void logV(String tag, String message, Object... params) {
        if (isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, String.format(message, params));
        }
    }

    /**
     * Check if input log level is available or not
     * @param tag       TAG name
     * @param level     Log.DEBUG or Log.VERBOSE
     * @return          true if level is lower than MAX_BLUETOOTH_LOG_LEVEL and Build type is not
     *                  for user.
     */
    public static boolean isLoggable(String tag, int level) {
        if (MAX_BLUETOOTH_LOG_LEVEL > level) {
            return false;
        }
        //TODO: cosider apply loggable method for each tags
        return /* Log.isLoggable(tag, level) && */ !Build.TYPE.equals("user");
    }
}
