/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.bluetooth.hfp;


import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.AudioPlaybackConfiguration;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import com.android.bluetooth.util.LogUtil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Defines methods used for synchronization between HFP and A2DP
 */
public class HeadsetA2dpSync {
    private static final String TAG = HeadsetA2dpSync.class.getSimpleName();

    private Handler mAudioPlaybackHandler;
    private boolean mAudioOnCall;
    private HeadsetSystemInterface mSystemInterface;
    private HeadsetService mHeadsetService;
    private ConcurrentHashMap<BluetoothDevice, Integer> mA2dpConnState =
            new ConcurrentHashMap<BluetoothDevice, Integer>();
    private int mA2dpSuspendTriggered;
    private BluetoothDevice mDummyDevice;

    public static final int A2DP_SUSPENDED_NOT_TRIGGERED = 0;
    public static final int A2DP_SUSPENDED_BY_CS_CALL = 1;
    public static final int A2DP_SUSPENDED_BY_VOIP_CALL = 2;
    public static final int A2DP_SUSPENDED_BY_VR = 3;

    public static final int A2DP_DISCONNECTED = 0;
    public static final int A2DP_CONNECTED = 1;
    public static final int A2DP_PLAYING = 2;

    private static final int AUDIO_CONFIG_CHANGE_TIMEOUT_MS = 200;
    private static final int AUDIO_CONFIG_CHANGE_TIMEOUT = 1001;
    private static final String DUMMY_ADDR = "00:11:22:33:44:55";

    HeadsetA2dpSync(HeadsetSystemInterface systemInterface, HeadsetService service) {
        mSystemInterface = systemInterface;
        mHeadsetService = service;
        mA2dpSuspendTriggered = A2DP_SUSPENDED_NOT_TRIGGERED;
        mDummyDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(DUMMY_ADDR);

        HandlerThread thread = new HandlerThread(TAG + "AudioPlaybackHandler");
        thread.start();
        mAudioPlaybackHandler = new Handler(thread.getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case AUDIO_CONFIG_CHANGE_TIMEOUT:
                        logD("mAudioOnCall = " + mAudioOnCall
                                + " Process resumeA2DP if any suspended!");
                        releaseA2DP(mDummyDevice);
                        break;
                }
                return true;
            }
        });

        mAudioOnCall = mSystemInterface.getAudioManager()
                .getActivePlaybackConfigurations()
                .stream()
                .filter(AudioPlaybackConfiguration::isActive)
                .collect(Collectors.toList())
                .stream()
                .anyMatch(config -> config.getAudioAttributes().getUsage()
                        == AudioAttributes.USAGE_VOICE_COMMUNICATION);
        logD("mAudioOnCall = " + mAudioOnCall);

        mSystemInterface.getAudioManager().registerAudioPlaybackCallback(
                new AudioManagerPlaybackListener(), mAudioPlaybackHandler);
    }

    private class AudioManagerPlaybackListener extends AudioManager.AudioPlaybackCallback {
        @Override
        public void onPlaybackConfigChanged(List<AudioPlaybackConfiguration> configs) {
            boolean currentAudioOnCall = configs.stream().anyMatch(config ->
                            config.isActive() && config.getAudioAttributes().getUsage()
                                    == AudioAttributes.USAGE_VOICE_COMMUNICATION);
            if (mAudioOnCall != currentAudioOnCall) {
                mAudioOnCall = currentAudioOnCall;
                logD("mAudioOnCall is changed to " + mAudioOnCall);

                if (mAudioOnCall) {
                    mAudioPlaybackHandler.removeMessages(AUDIO_CONFIG_CHANGE_TIMEOUT);
                } else {
                    if (mHeadsetService.isVirtualCallStarted()) {
                        logD("Ignore releasing A2DP. releasing will be through processCallState()");
                    } else {
                        mAudioPlaybackHandler.removeMessages(AUDIO_CONFIG_CHANGE_TIMEOUT);
                        mAudioPlaybackHandler.sendMessageDelayed(
                                mAudioPlaybackHandler.obtainMessage(AUDIO_CONFIG_CHANGE_TIMEOUT),
                                AUDIO_CONFIG_CHANGE_TIMEOUT_MS);
                    }
                }
            }
        }
    }

    /**
     * Check if any device is in PLAYING/CONNECTED state
     *
     * @return A2DP_DISCONNECTED : No Devices are connected
     *         A2DP_CONNECTED : All Devices are in connected state, none in playing state
     *         A2DP_PLAYING : At lease one device in playing state
     */
    public int isA2dpPlaying() {
        int a2dpState = A2DP_DISCONNECTED;
        for (Integer value : mA2dpConnState.values()) {
            if (value == A2DP_CONNECTED) {
                a2dpState = value;
            }
            if (value == A2DP_PLAYING) {
                a2dpState = value;
                break;
            }
        }
        logV("isA2dpPlaying = " + a2dpStateToString(a2dpState));
        return a2dpState;
    }

    /**
     * Check if device is in any of Call Started states
     *
     * @return true if device is in Call state or in Ringing state or in VirtualCall started state
     *              or in VoiceRecognition started state
     */
    public boolean isCallStarted() {
        boolean isInCall = mSystemInterface.isInCall() || mSystemInterface.isRinging()
                || mHeadsetService.isVirtualCallStarted() || mHeadsetService.isVRStarted();
        logV("isCallStarted = " + isInCall);
        return isInCall;
    }

    /**
     * Check if usage of attributes in current AudioPlaybackConfiguration has CALL
     * (#AudioAttributes.USAGE_VOICE_COMMUNICATION)
     *
     * @return mAudioOnCall
     */
    public boolean isAudioOnCall() {
        return mAudioOnCall;
    }

    /**
     * Performs suspend A2dp of device for a given reason. If a2dp state is {@link #A2DP_PLAYING},
     * this function will call "A2dpSuspended=true" for Audio to transfer to a2dp suspend state.
     *
     * @param reason Any of {@link #A2DP_SUSPENDED_BY_CS_CALL} {@link #A2DP_SUSPENDED_BY_VOIP_CALL}
     *               {@link #A2DP_SUSPENDED_BY_VOIP_CALL} {@link #A2DP_SUSPENDED_BY_VR}.
     *
     * @param device Bluetooth device
     * @return return true if we should wait for suspending result.
     *                false if we don't need to wait suspend
     */
    public boolean suspendA2DP(int reason, BluetoothDevice device) {
        int a2dpState = isA2dpPlaying();
        logD("suspendA2DP: " + "reason = " + suspendReasonToString(reason)
                + ", current state = " + suspendReasonToString(mA2dpSuspendTriggered)
                + ", device = " + device);
        if (mA2dpSuspendTriggered != A2DP_SUSPENDED_NOT_TRIGGERED) {
            return a2dpState == A2DP_PLAYING;
        } else {
            switch (a2dpState) {
                case A2DP_DISCONNECTED:
                    logD("A2DP not Connected. Nothing to do");
                    break;
                case A2DP_CONNECTED:
                    mA2dpSuspendTriggered = reason;
                    mSystemInterface.getAudioManager().setParameters("A2dpSuspended=true");
                    logD("A2DP Connected. Don't wait for suspend");
                    break;
                case A2DP_PLAYING:
                    mA2dpSuspendTriggered = reason;
                    mSystemInterface.getAudioManager().setParameters("A2dpSuspended=true");
                    logD("A2DP Playing. Wait for suspend");
                    break;
            }
            return a2dpState == A2DP_PLAYING;
        }
    }

    /**
     * Performs release A2dp of device. This function will call "A2dpSuspended=false" for Audio
     * to transfer to a2dp unsuspended state.
     *
     * @param device Bluetooth device
     */
    public void releaseA2DP(BluetoothDevice device) {
        logD("releaseA2DP: current state = " + suspendReasonToString(mA2dpSuspendTriggered)
                + ", device = " + device);
        if (mA2dpSuspendTriggered == A2DP_SUSPENDED_NOT_TRIGGERED) {
            Log.i(TAG, "No A2DP is suspended!");
            return;
        }
        if (mHeadsetService.isInCall() || mHeadsetService.isRinging()
                || mHeadsetService.isAudioOn()) {
            Log.i(TAG, "Call/Ring/SCO on for some other stateMachine, skip release a2dp");
            return;
        }
        if (mHeadsetService.isStateMachineOnDeferredAudioDisconnect()) {
            Log.i(TAG, "HeadsetStateMachine is on Deferred Audio Disconnection, skil release a2dp");
            return;
        }
        mA2dpSuspendTriggered = A2DP_SUSPENDED_NOT_TRIGGERED;
        mSystemInterface.getAudioManager().setParameters("A2dpSuspended=false");
    }

    void updateA2DPPlayingState(Intent intent) {
        int currState = intent.getIntExtra(BluetoothProfile.EXTRA_STATE,
                BluetoothA2dp.STATE_NOT_PLAYING);
        int prevState = intent.getIntExtra(BluetoothProfile.EXTRA_PREVIOUS_STATE,
                BluetoothA2dp.STATE_NOT_PLAYING);
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        logD("updateA2DPPlayingState: device = " + device + " transition = " +
                prevState + " -> " + currState);

        if (!mA2dpConnState.containsKey(device)) {
            Log.e(TAG, "Got A2DP state without Connection Update, this should not happen" + device);
            return;
        }

        switch (currState) {
            case BluetoothA2dp.STATE_NOT_PLAYING:
                mA2dpConnState.put(device, A2DP_CONNECTED);
                int a2dpState = isA2dpPlaying();
                if (a2dpState == A2DP_DISCONNECTED || a2dpState == A2DP_CONNECTED) {
                    mHeadsetService.sendA2dpStateChangeUpdate(a2dpState);
                }
                break;
            case BluetoothA2dp.STATE_PLAYING:
                mA2dpConnState.put(device, A2DP_PLAYING);
                if ((mHeadsetService.isInCall() || mHeadsetService.isRinging()) &&
                        mHeadsetService.getConnectedDevices().size() != 0) {
                    logD("CALL/Ring is active");
                    suspendA2DP(A2DP_SUSPENDED_BY_CS_CALL, mDummyDevice);
                }
                break;
        }
        logD("device = " + device + " state = " + mA2dpConnState.get(device));
    }

    public void updateA2DPConnectionState(Intent intent) {
        int currState = intent.getIntExtra(BluetoothProfile.EXTRA_STATE,
                BluetoothProfile.STATE_DISCONNECTED);
        int prevState = intent.getIntExtra(BluetoothProfile.EXTRA_PREVIOUS_STATE,
                BluetoothProfile.STATE_DISCONNECTED);
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

        logD("updateA2DPConnectionState: device = " + device + " transition = " +
                prevState + " -> " + currState);

        if (!mA2dpConnState.containsKey(device) &&
                (currState == BluetoothProfile.STATE_DISCONNECTED)) {
            Log.e(TAG, "Got A2DP disconnection from not in device entry, this should not happen");
            return;
        }

        switch (currState) {
            case BluetoothProfile.STATE_DISCONNECTED:
                mA2dpConnState.remove(device);
                break;
            case BluetoothProfile.STATE_DISCONNECTING:
            case BluetoothProfile.STATE_CONNECTING:
                mA2dpConnState.put(device, A2DP_CONNECTED);
                break;
            case BluetoothProfile.STATE_CONNECTED:
                mA2dpConnState.put(device, A2DP_CONNECTED);
                if (mHeadsetService.isInCall() || mHeadsetService.isRinging()) {
                    logD("CALL is active/ringing, A2DP got connected, suspending");
                    suspendA2DP(A2DP_SUSPENDED_BY_CS_CALL, mDummyDevice);
                }
                break;
        }
        logD("device = " + device + " state = " + mA2dpConnState.get(device));
    }

    private static String suspendReasonToString(int reason) {
        switch (reason) {
            case A2DP_SUSPENDED_NOT_TRIGGERED:
                return "A2DP_SUSPENDED_NOT_TRIGGERED";
            case A2DP_SUSPENDED_BY_CS_CALL:
                return "A2DP_SUSPENDED_BY_CS_CALL";
            case A2DP_SUSPENDED_BY_VOIP_CALL:
                return "A2DP_SUSPENDED_BY_VOIP_CALL";
            case A2DP_SUSPENDED_BY_VR:
                return "A2DP_SUSPENDED_BY_VR";
            default:
                break;
        }
        return Integer.toString(reason);
    }

    private static String a2dpStateToString(int state) {
        switch (state) {
            case A2DP_DISCONNECTED:
                return "A2DP_DISCONNECTED";
            case A2DP_CONNECTED:
                return "A2DP_CONNECTED";
            case A2DP_PLAYING:
                return "A2DP_PLAYING";
            default:
                break;
        }
        return Integer.toString(state);
    }

    private static void logD(String message) {
        LogUtil.logD(TAG, message);
    }

    private static void logV(String message) {
        LogUtil.logV(TAG, message);
    }
}