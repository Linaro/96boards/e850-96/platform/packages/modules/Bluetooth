/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "AvrcpTargetJni"

#include <base/bind.h>
#include <base/callback.h>
#include <map>
#include <mutex>
#include <shared_mutex>
#include <vector>

#include "avrcp.h"
#include "com_android_bluetooth.h"
#include "utils/Log.h"

using namespace bluetooth::avrcp;

namespace android {

// Static Variables
static MediaCallbacks* mServiceCallbacks;
static ServiceInterface* sServiceInterface;
static jobject mJavaInterface;
static std::shared_timed_mutex interface_mutex;
static std::shared_timed_mutex callbacks_mutex;

// Forward Declarations
static void sendMediaKeyEvent(const RawAddress&, int, KeyState);
static std::string getCurrentMediaId();
static SongInfo getSongInfo();
static PlayStatus getCurrentPlayStatus(const RawAddress& address, bool a2dp_event);
static std::vector<SongInfo> getNowPlayingList();
static uint16_t getCurrentPlayerId();
static std::vector<MediaPlayerInfo> getMediaPlayerList();
using SetAddressedPlayerCb = MediaInterface::SetAddressedPlayerCallback;
static void setAddressedPlayer(uint16_t player_id, SetAddressedPlayerCb);
using SetBrowsedPlayerCb = MediaInterface::SetBrowsedPlayerCallback;
static void setBrowsedPlayer(uint16_t player_id, SetBrowsedPlayerCb);
using GetFolderItemsCb = MediaInterface::FolderItemsCallback;
static void getFolderItems(uint16_t player_id, std::string media_id,
                           GetFolderItemsCb cb);
static void playItem(uint16_t player_id, bool now_playing,
                     std::string media_id);
static void setActiveDevice(const RawAddress& address);

static void volumeDeviceConnected(const RawAddress& address);
static void volumeDeviceConnected(
    const RawAddress& address,
    ::bluetooth::avrcp::VolumeInterface::VolumeChangedCb cb);
static void volumeDeviceDisconnected(const RawAddress& address);
static void setVolume(int8_t volume);
using PlayerSettingAttributesCallback = MediaInterface::PlayerSettingAttributesCallback;
static void listPlayerApplicationSettingAttributes(PlayerSettingAttributesCallback);
using PlayerSettingValuesCallback = MediaInterface::PlayerSettingValuesCallback;
static void listPlayerApplicationSettingValues(uint8_t, PlayerSettingValuesCallback);
using CurrentPlayerSettingValueCallback = MediaInterface::CurrentPlayerSettingValueCallback;
static void getCurrentPlayerApplicationSettingValue(uint8_t, std::vector<uint8_t>,
                                                    CurrentPlayerSettingValueCallback);
using SetPlayerApplicationSettingValueCallback =
                                        MediaInterface::SetPlayerApplicationSettingValueCallback;
static void setPlayerApplicationSettingValue(uint8_t, std::vector<uint8_t>, std::vector<uint8_t>,
                                             SetPlayerApplicationSettingValueCallback);
using PlayerApplicationSettingAttributeTextCallback =
                                      MediaInterface::PlayerApplicationSettingAttributeTextCallback;
static void getPlayerApplicationSettingAttributeText(uint8_t, std::vector<uint8_t>,
                                                     PlayerApplicationSettingAttributeTextCallback);
using PlayerApplicationSettingValueTextCallback =
                                        MediaInterface::PlayerApplicationSettingValueTextCallback;
static void getPlayerApplicationSettingValueText(uint8_t, uint8_t, std::vector<uint8_t>,
                                                 PlayerApplicationSettingValueTextCallback);
static std::vector<ApplicationSetting> getPlayerApplicationSettings();

// Local Variables
// TODO (apanicke): Use a map here to store the callback in order to
// support multi-browsing
SetBrowsedPlayerCb set_browsed_player_cb;
PlayerSettingAttributesCallback player_application_setting_attr_cb;
PlayerSettingValuesCallback player_application_setting_value_cb;
CurrentPlayerSettingValueCallback current_player_setting_value_cb;
SetPlayerApplicationSettingValueCallback set_player_application_setting_value_cb;
PlayerApplicationSettingAttributeTextCallback player_application_setting_attribute_text_cb;
PlayerApplicationSettingValueTextCallback player_application_setting_value_text_cb;
SetAddressedPlayerCb set_addressed_player_cb;
using map_entry = std::pair<std::string, GetFolderItemsCb>;
std::map<std::string, GetFolderItemsCb> get_folder_items_cb_map;
std::map<RawAddress, ::bluetooth::avrcp::VolumeInterface::VolumeChangedCb>
    volumeCallbackMap;

// TODO (apanicke): In the future, this interface should guarantee that
// all calls happen on the JNI Thread. Right now this is very difficult
// as it is hard to get a handle on the JNI thread from here.
class AvrcpMediaInterfaceImpl : public MediaInterface {
 public:
  void SendKeyEvent(const RawAddress& bdaddr, uint8_t key, KeyState state) {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    sendMediaKeyEvent(bdaddr, key, state);
  }

  void GetSongInfo(SongInfoCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    auto info = getSongInfo();
    cb.Run(info);
  }

  void GetPlayStatus(PlayStatusCallback cb, const RawAddress& address, bool a2dp_event) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    auto status = getCurrentPlayStatus(address, a2dp_event);
    cb.Run(status);
  }

  void GetNowPlayingList(NowPlayingCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    auto curr_song_id = getCurrentMediaId();
    auto now_playing_list = getNowPlayingList();
    cb.Run(curr_song_id, std::move(now_playing_list));
  }

  void GetMediaPlayerList(MediaListCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    uint16_t current_player = getCurrentPlayerId();
    auto player_list = getMediaPlayerList();
    cb.Run(current_player, std::move(player_list));
  }

  void GetFolderItems(uint16_t player_id, std::string media_id,
                      FolderItemsCallback folder_cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    getFolderItems(player_id, media_id, folder_cb);
  }

  void SetAddressedPlayer(uint16_t player_id,
                          SetAddressedPlayerCallback address_cb) override {
    setAddressedPlayer(player_id, address_cb);
  }

  void SetBrowsedPlayer(uint16_t player_id,
                        SetBrowsedPlayerCallback browse_cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    setBrowsedPlayer(player_id, browse_cb);
  }

  void RegisterUpdateCallback(MediaCallbacks* callback) override {
    // TODO (apanicke): Allow multiple registrations in the future
    mServiceCallbacks = callback;
  }

  void UnregisterUpdateCallback(MediaCallbacks* callback) override {
    mServiceCallbacks = nullptr;
  }

  void PlayItem(uint16_t player_id, bool now_playing,
                std::string media_id) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    playItem(player_id, now_playing, media_id);
  }

  void SetActiveDevice(const RawAddress& address) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    setActiveDevice(address);
  }

  // 6.5 Player Application Settings
  // PDU 0x11
  void ListPlayerApplicationSettingAttributes(PlayerSettingAttributesCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    listPlayerApplicationSettingAttributes(cb);
  }

  // PDU 0x12
  void ListPlayerApplicationSettingValues(uint8_t attr_id,
                                          PlayerSettingValuesCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    listPlayerApplicationSettingValues(attr_id, cb);
  }

  // PDU 0x13
  void GetCurrentPlayerApplicationSettingValue(uint8_t num_of_attrs,
                                               std::vector<uint8_t> attr_ids,
                                               CurrentPlayerSettingValueCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    getCurrentPlayerApplicationSettingValue(num_of_attrs, attr_ids, cb);
  }

  // PDU 0x14
  void SetPlayerApplicationSettingValue(uint8_t num_of_attrs,
                                        std::vector<uint8_t> attr_ids,
                                        std::vector<uint8_t> attr_values,
                                        SetPlayerApplicationSettingValueCallback cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    setPlayerApplicationSettingValue(num_of_attrs, attr_ids, attr_values, cb);
  }

  // PDU 0x15
  void GetPlayerApplicationSettingAttributeText(uint8_t num_of_attrs, std::vector<uint8_t> attr_ids,
                                                PlayerApplicationSettingAttributeTextCallback cb) {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    getPlayerApplicationSettingAttributeText(num_of_attrs, attr_ids, cb);
  }

  // PDU 0x16
  void GetPlayerApplicationSettingValueText(uint8_t attr_id, uint8_t num_of_attrs,
                                            std::vector<uint8_t> attr_values,
                                            PlayerApplicationSettingValueTextCallback cb) {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    getPlayerApplicationSettingValueText(attr_id, num_of_attrs, attr_values, cb);
  }

  // To handle PlayerSettingApplication Notification
  void GetPlayerApplicationSettings(PlayerApplicationSettingsCallback cb) {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    auto list = getPlayerApplicationSettings();
    std::vector<uint8_t> attr_ids;
    std::vector<uint8_t> attr_values;
    for (auto item : list) {
      attr_ids.push_back(item.attr_id);
      attr_values.push_back(item.attr_value);
    }
    cb.Run(list.size(), attr_ids, attr_values);
  }
};
static AvrcpMediaInterfaceImpl mAvrcpInterface;

class VolumeInterfaceImpl : public VolumeInterface {
 public:
  void DeviceConnected(const RawAddress& bdaddr) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    volumeDeviceConnected(bdaddr);
  }

  void DeviceConnected(const RawAddress& bdaddr, VolumeChangedCb cb) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    volumeDeviceConnected(bdaddr, cb);
  }

  void DeviceDisconnected(const RawAddress& bdaddr) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    volumeDeviceDisconnected(bdaddr);
  }

  void SetVolume(int8_t volume) override {
    std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
    setVolume(volume);
  }
};
static VolumeInterfaceImpl mVolumeInterface;

static jmethodID method_getCurrentSongInfo;
static jmethodID method_getPlaybackStatus;
static jmethodID method_sendMediaKeyEvent;
static jmethodID method_listPlayerApplicationSettingAttr;
static jmethodID method_listPlayerApplicationAttrValues;
static jmethodID method_getPlayerAttrValues;
static jmethodID method_setPlayerAttrSettings;
static jmethodID method_getPlayerApplicationSettingsText;
static jmethodID method_getPlayerApplicationValueText;

static jmethodID method_getCurrentMediaId;
static jmethodID method_getNowPlayingList;

static jmethodID method_setAddressedPlayer;
static jmethodID method_setBrowsedPlayer;
static jmethodID method_getCurrentPlayerId;
static jmethodID method_getMediaPlayerList;
static jmethodID method_getPlayerApplicationSetting;
static jmethodID method_getFolderItemsRequest;
static jmethodID method_playItem;

static jmethodID method_setActiveDevice;

static jmethodID method_volumeDeviceConnected;
static jmethodID method_volumeDeviceDisconnected;

static jmethodID method_setVolume;

static void classInitNative(JNIEnv* env, jclass clazz) {
  method_getCurrentSongInfo = env->GetMethodID(
      clazz, "getCurrentSongInfo", "()Lcom/android/bluetooth/audio_util/Metadata;");

  method_getPlaybackStatus = env->GetMethodID(
      clazz, "getPlayStatus", "(Ljava/lang/String;Z)Lcom/android/bluetooth/audio_util/PlayStatus;");

  method_listPlayerApplicationSettingAttr =
      env->GetMethodID(clazz, "listPlayerApplicationAttributes", "()V");

  method_listPlayerApplicationAttrValues =
      env->GetMethodID(clazz , "listPlayerApplicationAttributeValues", "(B)V");

  method_getPlayerAttrValues =
      env->GetMethodID(clazz, "getPlayerAttributeValues", "(B[B)V");

  method_setPlayerAttrSettings =
      env->GetMethodID(clazz, "setPlayerAttributeSettings","(B[B[B)V");

  method_getPlayerApplicationSettingsText =
      env->GetMethodID(clazz, "getPlayerApplicationSettingsText", "(B[B)V");

  method_getPlayerApplicationValueText =
      env->GetMethodID(clazz, "getPlayerApplicationValueText", "(BB[B)V");

  method_sendMediaKeyEvent =
      env->GetMethodID(clazz, "sendMediaKeyEvent", "(Ljava/lang/String;IZ)V");

  method_getCurrentMediaId =
      env->GetMethodID(clazz, "getCurrentMediaId", "()Ljava/lang/String;");

  method_getNowPlayingList =
      env->GetMethodID(clazz, "getNowPlayingList", "()Ljava/util/List;");

  method_getCurrentPlayerId =
      env->GetMethodID(clazz, "getCurrentPlayerId", "()I");

  method_getMediaPlayerList =
      env->GetMethodID(clazz, "getMediaPlayerList", "()Ljava/util/List;");

  method_getPlayerApplicationSetting =
      env->GetMethodID(clazz, "getPlayerApplicationSettings", "()Ljava/util/List;");

  method_setAddressedPlayer = env->GetMethodID(clazz, "setAddressedPlayer", "(I)V");

  method_setBrowsedPlayer = env->GetMethodID(clazz, "setBrowsedPlayer", "(I)V");

  method_getFolderItemsRequest = env->GetMethodID(
      clazz, "getFolderItemsRequest", "(ILjava/lang/String;)V");

  method_playItem =
      env->GetMethodID(clazz, "playItem", "(IZLjava/lang/String;)V");

  method_setActiveDevice =
      env->GetMethodID(clazz, "setActiveDevice", "(Ljava/lang/String;)V");

  // Volume Management functions
  method_volumeDeviceConnected =
      env->GetMethodID(clazz, "deviceConnected", "(Ljava/lang/String;Z)V");

  method_volumeDeviceDisconnected =
      env->GetMethodID(clazz, "deviceDisconnected", "(Ljava/lang/String;)V");

  method_setVolume = env->GetMethodID(clazz, "setVolume", "(I)V");

  ALOGI("%s: AvrcpTargetJni initialized!", __func__);
}

static void initNative(JNIEnv* env, jobject object) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  std::unique_lock<std::shared_timed_mutex> callbacks_lock(callbacks_mutex);
  mJavaInterface = env->NewGlobalRef(object);

  sServiceInterface = getBluetoothInterface()->get_avrcp_service();
  sServiceInterface->Init(&mAvrcpInterface, &mVolumeInterface);
}

static void registerBipServerNative(JNIEnv* env, jobject object,
                                    jint l2cap_psm) {
  ALOGD("%s: l2cap_psm=%d", __func__, (int)l2cap_psm);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (sServiceInterface == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }
  sServiceInterface->RegisterBipServer((int)l2cap_psm);
}

static void unregisterBipServerNative(JNIEnv* env, jobject object) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (sServiceInterface == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }
  sServiceInterface->UnregisterBipServer();
}

static void sendMediaUpdateNative(JNIEnv* env, jobject object,
                                  jboolean metadata, jboolean state,
                                  jboolean queue) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }

  mServiceCallbacks->SendMediaUpdate(metadata == JNI_TRUE, state == JNI_TRUE,
                                     queue == JNI_TRUE);
}

static void sendFolderUpdateNative(JNIEnv* env, jobject object,
                                   jboolean available_players,
                                   jboolean addressed_player, jboolean uids) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }

  mServiceCallbacks->SendFolderUpdate(available_players == JNI_TRUE,
                                      addressed_player == JNI_TRUE,
                                      uids == JNI_TRUE);
}

static void sendA2dpStatusChangeNative(JNIEnv* env, jobject object,
                                       jstring address, jboolean is_playing) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }

  const char* tmp_addr = env->GetStringUTFChars(address, 0);
  RawAddress bdaddr;
  bool success = RawAddress::FromString(tmp_addr, bdaddr);
  env->ReleaseStringUTFChars(address, tmp_addr);

  if (!success) return;

  mServiceCallbacks->SendA2dpStatusChange(bdaddr, is_playing == JNI_TRUE);
}

static void sendPlayerApplicationSettingsUpdateNative(JNIEnv* env, jobject object) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);

  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }

  mServiceCallbacks->SendPlayerApplicationSettingsUpdate();
}

static void cleanupNative(JNIEnv* env, jobject object) {
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  std::unique_lock<std::shared_timed_mutex> callbacks_lock(callbacks_mutex);

  get_folder_items_cb_map.clear();
  volumeCallbackMap.clear();

  sServiceInterface->Cleanup();
  env->DeleteGlobalRef(mJavaInterface);
  mJavaInterface = nullptr;
  mServiceCallbacks = nullptr;
  sServiceInterface = nullptr;
}

jboolean connectDeviceNative(JNIEnv* env, jobject object, jstring address) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return JNI_FALSE;
  }

  const char* tmp_addr = env->GetStringUTFChars(address, 0);
  RawAddress bdaddr;
  bool success = RawAddress::FromString(tmp_addr, bdaddr);
  env->ReleaseStringUTFChars(address, tmp_addr);

  if (!success) return JNI_FALSE;

  return sServiceInterface->ConnectDevice(bdaddr) == true ? JNI_TRUE
                                                          : JNI_FALSE;
}

jboolean disconnectDeviceNative(JNIEnv* env, jobject object, jstring address) {
  ALOGD("%s", __func__);
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return JNI_FALSE;
  }

  const char* tmp_addr = env->GetStringUTFChars(address, 0);
  RawAddress bdaddr;
  bool success = RawAddress::FromString(tmp_addr, bdaddr);
  env->ReleaseStringUTFChars(address, tmp_addr);

  if (!success) return JNI_FALSE;

  return sServiceInterface->DisconnectDevice(bdaddr) == true ? JNI_TRUE
                                                             : JNI_FALSE;
}

static void sendMediaKeyEvent(const RawAddress& address, int key, KeyState state) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;
  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  sCallbackEnv->CallVoidMethod(
      mJavaInterface, method_sendMediaKeyEvent, j_bdaddr, key,
      state == KeyState::PUSHED ? JNI_TRUE : JNI_FALSE);
}

static std::string getImageHandleFromJavaObj(JNIEnv* env, jobject image) {
  std::string handle;

  if (image == nullptr) return handle;

  jclass class_image = env->GetObjectClass(image);
  jmethodID method_getImageHandle =
      env->GetMethodID(class_image, "getImageHandle", "()Ljava/lang/String;");
  jstring imageHandle = (jstring) env->CallObjectMethod(
      image, method_getImageHandle);
  if (imageHandle == nullptr) {
    return handle;
  }

  const char* value = env->GetStringUTFChars(imageHandle, nullptr);
  handle = std::string(value);
  env->ReleaseStringUTFChars(imageHandle, value);
  env->DeleteLocalRef(imageHandle);
  return handle;
}

static SongInfo getSongInfoFromJavaObj(JNIEnv* env, jobject metadata) {
  SongInfo info;

  if (metadata == nullptr) return info;

  jclass class_metadata = env->GetObjectClass(metadata);
  jfieldID field_mediaId =
      env->GetFieldID(class_metadata, "mediaId", "Ljava/lang/String;");
  jfieldID field_title =
      env->GetFieldID(class_metadata, "title", "Ljava/lang/String;");
  jfieldID field_artist =
      env->GetFieldID(class_metadata, "artist", "Ljava/lang/String;");
  jfieldID field_album =
      env->GetFieldID(class_metadata, "album", "Ljava/lang/String;");
  jfieldID field_trackNum =
      env->GetFieldID(class_metadata, "trackNum", "Ljava/lang/String;");
  jfieldID field_numTracks =
      env->GetFieldID(class_metadata, "numTracks", "Ljava/lang/String;");
  jfieldID field_genre =
      env->GetFieldID(class_metadata, "genre", "Ljava/lang/String;");
  jfieldID field_playingTime =
      env->GetFieldID(class_metadata, "duration", "Ljava/lang/String;");
  jfieldID field_image =
      env->GetFieldID(class_metadata, "image", "Lcom/android/bluetooth/audio_util/Image;");

  jstring jstr = (jstring)env->GetObjectField(metadata, field_mediaId);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.media_id = std::string(value);
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.media_id = std::string();
  }

  jstr = (jstring)env->GetObjectField(metadata, field_title);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::TITLE, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::TITLE, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_artist);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::ARTIST_NAME, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::ARTIST_NAME, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_album);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::ALBUM_NAME, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::ALBUM_NAME, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_trackNum);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::TRACK_NUMBER, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::TRACK_NUMBER, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_numTracks);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::TOTAL_NUMBER_OF_TRACKS, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::TOTAL_NUMBER_OF_TRACKS, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_genre);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::GENRE, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::GENRE, std::string()));
  }

  jstr = (jstring)env->GetObjectField(metadata, field_playingTime);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.attributes.insert(
        AttributeEntry(Attribute::PLAYING_TIME, std::string(value)));
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::PLAYING_TIME, std::string()));
  }

  jobject object_image = env->GetObjectField(metadata, field_image);
  if (object_image != nullptr) {
    std::string imageHandle = getImageHandleFromJavaObj(env, object_image);
    if (!imageHandle.empty()) {
      info.attributes.insert(
          AttributeEntry(Attribute::DEFAULT_COVER_ART, imageHandle));
    }
    env->DeleteLocalRef(object_image);
  } else {
    info.attributes.insert(
        AttributeEntry(Attribute::DEFAULT_COVER_ART, std::string()));
  }

  return info;
}

static FolderInfo getFolderInfoFromJavaObj(JNIEnv* env, jobject folder) {
  FolderInfo info;

  jclass class_folder = env->GetObjectClass(folder);
  jfieldID field_mediaId =
      env->GetFieldID(class_folder, "mediaId", "Ljava/lang/String;");
  jfieldID field_isPlayable = env->GetFieldID(class_folder, "isPlayable", "Z");
  jfieldID field_name =
      env->GetFieldID(class_folder, "title", "Ljava/lang/String;");

  jstring jstr = (jstring)env->GetObjectField(folder, field_mediaId);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.media_id = std::string(value);
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  }

  info.is_playable = env->GetBooleanField(folder, field_isPlayable) == JNI_TRUE;

  jstr = (jstring)env->GetObjectField(folder, field_name);
  if (jstr != nullptr) {
    const char* value = env->GetStringUTFChars(jstr, nullptr);
    info.name = std::string(value);
    env->ReleaseStringUTFChars(jstr, value);
    env->DeleteLocalRef(jstr);
  }

  return info;
}

static SongInfo getSongInfo() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return SongInfo();

  jobject metadata =
      sCallbackEnv->CallObjectMethod(mJavaInterface, method_getCurrentSongInfo);
  SongInfo info = getSongInfoFromJavaObj(sCallbackEnv.get(), metadata);
  sCallbackEnv->DeleteLocalRef(metadata);
  return info;
}

static PlayStatus getCurrentPlayStatus(const RawAddress& address, bool a2dp_event) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return PlayStatus();

  PlayStatus status = {0, 0, PlayState::STOPPED};
  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  jobject playStatus =
      sCallbackEnv->CallObjectMethod(mJavaInterface,
                                     method_getPlaybackStatus, j_bdaddr, a2dp_event);

  if (playStatus == nullptr) {
    ALOGE("%s: Got a null play status", __func__);
    return status;
  }

  jclass class_playStatus = sCallbackEnv->GetObjectClass(playStatus);
  jfieldID field_position =
      sCallbackEnv->GetFieldID(class_playStatus, "position", "J");
  jfieldID field_duration =
      sCallbackEnv->GetFieldID(class_playStatus, "duration", "J");
  jfieldID field_state =
      sCallbackEnv->GetFieldID(class_playStatus, "state", "B");

  status.position = sCallbackEnv->GetLongField(playStatus, field_position);
  status.duration = sCallbackEnv->GetLongField(playStatus, field_duration);
  status.state = (PlayState)sCallbackEnv->GetByteField(playStatus, field_state);

  sCallbackEnv->DeleteLocalRef(playStatus);

  return status;
}

static void listPlayerApplicationSettingAttributes(PlayerSettingAttributesCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  player_application_setting_attr_cb = cb;
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_listPlayerApplicationSettingAttr);
}

static void listPlayerApplicationSettingValues(uint8_t attr_id, PlayerSettingValuesCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  player_application_setting_value_cb = cb;
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_listPlayerApplicationAttrValues, attr_id);
}

static void getCurrentPlayerApplicationSettingValue(uint8_t num_of_attrs,
                                                    std::vector<uint8_t> attr_ids,
                                                    CurrentPlayerSettingValueCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  ScopedLocalRef<jbyteArray> jAttrIds(sCallbackEnv.get(),
                                        sCallbackEnv->NewByteArray(num_of_attrs));
  if (!jAttrIds.get()) {
    ALOGE("Fail to new jbyteArray for attr_ids");
    return;
  }

  current_player_setting_value_cb = cb;
  uint8_t *attr = &attr_ids[0];

  sCallbackEnv->SetByteArrayRegion(jAttrIds.get(), 0, num_of_attrs, (jbyte *)attr);
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_getPlayerAttrValues,
                                 num_of_attrs, jAttrIds.get());
}

static void setPlayerApplicationSettingValue(uint8_t num_of_attrs,
                                             std::vector<uint8_t> attr_ids,
                                             std::vector<uint8_t> attr_values,
                                             SetPlayerApplicationSettingValueCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  ScopedLocalRef<jbyteArray> jAttrIds(sCallbackEnv.get(),
                                          sCallbackEnv->NewByteArray(num_of_attrs));
  if (!jAttrIds.get()) {
    ALOGE("Fail to new jbyteArray for attr_ids");
    return;
  }

  ScopedLocalRef<jbyteArray> jAttrValues(sCallbackEnv.get(),
                                            sCallbackEnv->NewByteArray(num_of_attrs));
  if (!jAttrValues.get()) {
    ALOGE("Fail to new jbyteArray for attr_values");
    return;
  }

  set_player_application_setting_value_cb = cb;
  uint8_t *attrIds = &attr_ids[0];
  uint8_t *attrValues = &attr_values[0];

  sCallbackEnv->SetByteArrayRegion(jAttrIds.get(), 0, num_of_attrs, (jbyte *)attrIds);
  sCallbackEnv->SetByteArrayRegion(jAttrValues.get(), 0, num_of_attrs, (jbyte *)attrValues);
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_setPlayerAttrSettings,
                               num_of_attrs, jAttrIds.get(), jAttrValues.get());
}

static void getPlayerApplicationSettingAttributeText(uint8_t num_of_attrs,
                                                 std::vector<uint8_t> attr_ids,
                                                 PlayerApplicationSettingAttributeTextCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  ScopedLocalRef<jbyteArray> jAttrIds(sCallbackEnv.get(),
                                              sCallbackEnv->NewByteArray(num_of_attrs));
  if (!jAttrIds.get()) {
    ALOGE("Fail to new jbyteArray for attr_ids");
    return;
  }

  player_application_setting_attribute_text_cb = cb;
  uint8_t *attrIds = &attr_ids[0];

  sCallbackEnv->SetByteArrayRegion(jAttrIds.get(), 0, num_of_attrs, (jbyte *)attrIds);
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_getPlayerApplicationSettingsText,
                               num_of_attrs, jAttrIds.get());
}

static void getPlayerApplicationSettingValueText(uint8_t attr_id, uint8_t num_of_attrs,
                                                 std::vector<uint8_t> attr_values,
                                                 PlayerApplicationSettingValueTextCallback cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  ScopedLocalRef<jbyteArray> jAttrValues(sCallbackEnv.get(),
                                                sCallbackEnv->NewByteArray(num_of_attrs));
  if (!jAttrValues.get()) {
    ALOGE("Fail to new jbyteArray for attr_values");
    return;
  }

  player_application_setting_value_text_cb = cb;
  uint8_t *attrValues = &attr_values[0];

  sCallbackEnv->SetByteArrayRegion(jAttrValues.get(), 0, num_of_attrs, (jbyte *)attrValues);
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_getPlayerApplicationValueText,
                                 attr_id, num_of_attrs, jAttrValues.get());
}

static std::string getCurrentMediaId() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return "";

  jstring media_id = (jstring)sCallbackEnv->CallObjectMethod(
      mJavaInterface, method_getCurrentMediaId);
  if (media_id == nullptr) {
    ALOGE("%s: Got a null media ID", __func__);
    return "";
  }

  const char* value = sCallbackEnv->GetStringUTFChars(media_id, nullptr);
  std::string ret(value);
  sCallbackEnv->ReleaseStringUTFChars(media_id, value);
  sCallbackEnv->DeleteLocalRef(media_id);
  return ret;
}

static std::vector<SongInfo> getNowPlayingList() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return std::vector<SongInfo>();

  jobject song_list =
      sCallbackEnv->CallObjectMethod(mJavaInterface, method_getNowPlayingList);
  if (song_list == nullptr) {
    ALOGE("%s: Got a null now playing list", __func__);
    return std::vector<SongInfo>();
  }

  jclass class_list = sCallbackEnv->GetObjectClass(song_list);
  jmethodID method_get =
      sCallbackEnv->GetMethodID(class_list, "get", "(I)Ljava/lang/Object;");
  jmethodID method_size = sCallbackEnv->GetMethodID(class_list, "size", "()I");

  auto size = sCallbackEnv->CallIntMethod(song_list, method_size);
  if (size == 0) {
    sCallbackEnv->DeleteLocalRef(song_list);
    return std::vector<SongInfo>();
  }
  std::vector<SongInfo> ret;
  for (int i = 0; i < size; i++) {
    jobject song = sCallbackEnv->CallObjectMethod(song_list, method_get, i);
    ret.push_back(getSongInfoFromJavaObj(sCallbackEnv.get(), song));
    sCallbackEnv->DeleteLocalRef(song);
  }

  sCallbackEnv->DeleteLocalRef(song_list);

  return ret;
}

static uint16_t getCurrentPlayerId() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return 0u;

  jint id =
      sCallbackEnv->CallIntMethod(mJavaInterface, method_getCurrentPlayerId);

  return (static_cast<int>(id) & 0xFFFF);
}

static std::vector<MediaPlayerInfo> getMediaPlayerList() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface)
    return std::vector<MediaPlayerInfo>();

  jobject player_list = (jobject)sCallbackEnv->CallObjectMethod(
      mJavaInterface, method_getMediaPlayerList);

  if (player_list == nullptr) {
    ALOGE("%s: Got a null media player list", __func__);
    return std::vector<MediaPlayerInfo>();
  }

  jclass class_list = sCallbackEnv->GetObjectClass(player_list);
  jmethodID method_get =
      sCallbackEnv->GetMethodID(class_list, "get", "(I)Ljava/lang/Object;");
  jmethodID method_size = sCallbackEnv->GetMethodID(class_list, "size", "()I");

  jint list_size = sCallbackEnv->CallIntMethod(player_list, method_size);
  if (list_size == 0) {
    sCallbackEnv->DeleteLocalRef(player_list);
    return std::vector<MediaPlayerInfo>();
  }

  jobject player_info =
      sCallbackEnv->CallObjectMethod(player_list, method_get, 0);
  jclass class_playerInfo = sCallbackEnv->GetObjectClass(player_info);
  jfieldID field_playerId =
      sCallbackEnv->GetFieldID(class_playerInfo, "id", "I");
  jfieldID field_name =
      sCallbackEnv->GetFieldID(class_playerInfo, "name", "Ljava/lang/String;");
  jfieldID field_browsable =
      sCallbackEnv->GetFieldID(class_playerInfo, "browsable", "Z");

  std::vector<MediaPlayerInfo> ret_list;
  for (jsize i = 0; i < list_size; i++) {
    jobject player = sCallbackEnv->CallObjectMethod(player_list, method_get, i);

    MediaPlayerInfo temp;
    temp.id = sCallbackEnv->GetIntField(player, field_playerId);

    jstring jstr = (jstring)sCallbackEnv->GetObjectField(player, field_name);
    if (jstr != nullptr) {
      const char* value = sCallbackEnv->GetStringUTFChars(jstr, nullptr);
      temp.name = std::string(value);
      sCallbackEnv->ReleaseStringUTFChars(jstr, value);
      sCallbackEnv->DeleteLocalRef(jstr);
    }

    temp.browsing_supported =
        sCallbackEnv->GetBooleanField(player, field_browsable) == JNI_TRUE
            ? true
            : false;

    ret_list.push_back(std::move(temp));
    sCallbackEnv->DeleteLocalRef(player);
  }

  sCallbackEnv->DeleteLocalRef(player_info);
  sCallbackEnv->DeleteLocalRef(player_list);

  return ret_list;
}

static std::vector<ApplicationSetting> getPlayerApplicationSettings() {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface)
    return std::vector<ApplicationSetting>();

  jobject player_application_settings_list = (jobject)sCallbackEnv->CallObjectMethod(
      mJavaInterface, method_getPlayerApplicationSetting);

  if (player_application_settings_list == nullptr) {
    ALOGE("%s: Got a null player application settings list", __func__);
    return std::vector<ApplicationSetting>();
  }

  jclass class_list = sCallbackEnv->GetObjectClass(player_application_settings_list);
  jmethodID method_get =
      sCallbackEnv->GetMethodID(class_list, "get", "(I)Ljava/lang/Object;");
  jmethodID method_size = sCallbackEnv->GetMethodID(class_list, "size", "()I");

  jint list_size = sCallbackEnv->CallIntMethod(player_application_settings_list, method_size);
  if (list_size == 0) {
    return std::vector<ApplicationSetting>();
  }

  jclass class_playerApplicationSetting = sCallbackEnv->GetObjectClass(
      sCallbackEnv->CallObjectMethod(player_application_settings_list, method_get, 0));
  jfieldID field_attrId =
      sCallbackEnv->GetFieldID(class_playerApplicationSetting, "attrId", "B");
  jfieldID field_attrValue =
      sCallbackEnv->GetFieldID(class_playerApplicationSetting, "attrValue", "B");

  std::vector<ApplicationSetting> player_application_settings;
  for (jsize i = 0; i < list_size; i++) {
    jobject playerApplicationSetting =
        sCallbackEnv->CallObjectMethod(player_application_settings_list, method_get, i);
    ApplicationSetting temp;
    temp.attr_id = sCallbackEnv->GetByteField(playerApplicationSetting, field_attrId);
    temp.attr_value = sCallbackEnv->GetByteField(playerApplicationSetting, field_attrValue);

    player_application_settings.push_back(std::move(temp));
    sCallbackEnv->DeleteLocalRef(playerApplicationSetting);
  }

  return player_application_settings;
}

static void setAddressedPlayer(uint16_t player_id, SetAddressedPlayerCb cb) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  set_addressed_player_cb = cb;
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_setAddressedPlayer,
                               player_id);
}

static void setAddressedPlayerResponseNative(JNIEnv* env, jobject object,
                                             jint response) {
  ALOGD("%s", __func__);
  set_addressed_player_cb.Run(response);
}

static void setBrowsedPlayer(uint16_t player_id, SetBrowsedPlayerCb cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  set_browsed_player_cb = cb;
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_setBrowsedPlayer,
                               player_id);
}

static void setBrowsedPlayerResponseNative(JNIEnv* env, jobject object,
                                           jint player_id, jboolean success,
                                           jstring root_id, jint num_items) {
  ALOGD("%s", __func__);

  std::string root;
  if (root_id != nullptr) {
    const char* value = env->GetStringUTFChars(root_id, nullptr);
    root = std::string(value);
    env->ReleaseStringUTFChars(root_id, value);
  }

  set_browsed_player_cb.Run(success == JNI_TRUE, root, num_items);
}

static void getFolderItemsResponseNative(JNIEnv* env, jobject object,
                                         jstring parent_id, jobject list) {
  ALOGD("%s", __func__);

  std::string id;
  if (parent_id != nullptr) {
    const char* value = env->GetStringUTFChars(parent_id, nullptr);
    id = std::string(value);
    env->ReleaseStringUTFChars(parent_id, value);
  }

  // TODO (apanicke): Right now browsing will fail on a second device if two
  // devices browse the same folder. Use a MultiMap to fix this behavior so
  // that both callbacks can be handled with one lookup if a request comes
  // for a folder that is already trying to be looked at.
  if (get_folder_items_cb_map.find(id) == get_folder_items_cb_map.end()) {
    ALOGE("Could not find response callback for the request of \"%s\"",
          id.c_str());
    return;
  }

  auto callback = get_folder_items_cb_map.find(id)->second;
  get_folder_items_cb_map.erase(id);

  if (list == nullptr) {
    ALOGE("%s: Got a null get folder items response list", __func__);
    callback.Run(std::vector<ListItem>());
    return;
  }

  jclass class_list = env->GetObjectClass(list);
  jmethodID method_get =
      env->GetMethodID(class_list, "get", "(I)Ljava/lang/Object;");
  jmethodID method_size = env->GetMethodID(class_list, "size", "()I");

  jint list_size = env->CallIntMethod(list, method_size);
  if (list_size == 0) {
    callback.Run(std::vector<ListItem>());
    return;
  }

  jobject list_item = env->CallObjectMethod(list, method_get, 0);
  jclass class_listItem = env->GetObjectClass(list_item);
  jfieldID field_isFolder = env->GetFieldID(class_listItem, "isFolder", "Z");
  jfieldID field_folder = env->GetFieldID(
      class_listItem, "folder", "Lcom/android/bluetooth/audio_util/Folder;");
  jfieldID field_song = env->GetFieldID(
      class_listItem, "song", "Lcom/android/bluetooth/audio_util/Metadata;");

  std::vector<ListItem> ret_list;
  for (jsize i = 0; i < list_size; i++) {
    jobject item = env->CallObjectMethod(list, method_get, i);

    bool is_folder = env->GetBooleanField(item, field_isFolder) == JNI_TRUE;

    if (is_folder) {
      jobject folder = env->GetObjectField(item, field_folder);
      ListItem temp = {ListItem::FOLDER,
                       getFolderInfoFromJavaObj(env, folder),
                       SongInfo()};
      ret_list.push_back(temp);
      env->DeleteLocalRef(folder);
    } else {
      jobject song = env->GetObjectField(item, field_song);
      ListItem temp = {ListItem::SONG, FolderInfo(),
                       getSongInfoFromJavaObj(env, song)};
      ret_list.push_back(temp);
      env->DeleteLocalRef(song);
    }
    env->DeleteLocalRef(item);
  }

  env->DeleteLocalRef(list_item);

  callback.Run(std::move(ret_list));
}

static void getFolderItems(uint16_t player_id, std::string media_id,
                           GetFolderItemsCb cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  // TODO (apanicke): Fix a potential media_id collision if two media players
  // use the same media_id scheme or two devices browse the same content.
  get_folder_items_cb_map.insert(map_entry(media_id, cb));

  jstring j_media_id = sCallbackEnv->NewStringUTF(media_id.c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_getFolderItemsRequest,
                               player_id, j_media_id);
}

static void playItem(uint16_t player_id, bool now_playing,
                     std::string media_id) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  jstring j_media_id = sCallbackEnv->NewStringUTF(media_id.c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_playItem, player_id,
                               now_playing ? JNI_TRUE : JNI_FALSE, j_media_id);
}

static void setActiveDevice(const RawAddress& address) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_setActiveDevice,
                               j_bdaddr);
}

static void volumeDeviceConnected(const RawAddress& address) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_volumeDeviceConnected,
                               j_bdaddr, JNI_FALSE);
}

static void volumeDeviceConnected(
    const RawAddress& address,
    ::bluetooth::avrcp::VolumeInterface::VolumeChangedCb cb) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  volumeCallbackMap.emplace(address, cb);

  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_volumeDeviceConnected,
                               j_bdaddr, JNI_TRUE);
}

static void volumeDeviceDisconnected(const RawAddress& address) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  volumeCallbackMap.erase(address);

  jstring j_bdaddr = sCallbackEnv->NewStringUTF(address.ToString().c_str());
  sCallbackEnv->CallVoidMethod(mJavaInterface, method_volumeDeviceDisconnected,
                               j_bdaddr);
}

static void sendVolumeChangedNative(JNIEnv* env, jobject object,
                                    jstring address, jint volume) {
  const char* tmp_addr = env->GetStringUTFChars(address, 0);
  RawAddress bdaddr;
  bool success = RawAddress::FromString(tmp_addr, bdaddr);
  env->ReleaseStringUTFChars(address, tmp_addr);

  if (!success) return;

  ALOGD("%s", __func__);
  if (volumeCallbackMap.find(bdaddr) != volumeCallbackMap.end()) {
    volumeCallbackMap.find(bdaddr)->second.Run(volume & 0x7F);
  }
}

static void setVolume(int8_t volume) {
  ALOGD("%s", __func__);
  CallbackEnv sCallbackEnv(__func__);
  if (!sCallbackEnv.valid() || !mJavaInterface) return;

  sCallbackEnv->CallVoidMethod(mJavaInterface, method_setVolume, volume);
}

static void setBipClientStatusNative(JNIEnv* env, jobject object,
                                    jstring address, jboolean connected) {
  std::unique_lock<std::shared_timed_mutex> interface_lock(interface_mutex);
  if (mServiceCallbacks == nullptr) {
    ALOGW("%s: Service not loaded.", __func__);
    return;
  }

  const char* tmp_addr = env->GetStringUTFChars(address, 0);
  RawAddress bdaddr;
  bool success = RawAddress::FromString(tmp_addr, bdaddr);
  env->ReleaseStringUTFChars(address, tmp_addr);

  if (!success) return;

  bool status = (connected == JNI_TRUE);
  sServiceInterface->SetBipClientStatus(bdaddr, status);
}

//JNI Method called to Respond to PDU 0x11
static jboolean getListPlayerApplicationAttrRspNative(JNIEnv *env, jobject object,
                                                      jbyte num_attrs, jbyteArray attr_ids) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  if (num_attrs > 16) {  // MAX ATTR SIZE = 16
    ALOGE("number of attributes exceed maximum");
    return JNI_FALSE;
  }
  std::vector<uint8_t> attrIds;
  jbyte *attr = env->GetByteArrayElements(attr_ids, NULL);
  if (!attr) {
    jniThrowIOException(env, EINVAL);
    return JNI_FALSE ;
  }

  for (int i = 0; i < num_attrs; i++) {
    attrIds.push_back(std::move((uint8_t)attr[i]));
  }

  env->ReleaseByteArrayElements(attr_ids, attr, 0);
  player_application_setting_attr_cb.Run(num_attrs, attrIds);

  return JNI_TRUE;
}

//JNI Method called to Respond to PDU 0x12
static jboolean getPlayerApplicationValueRspNative(JNIEnv *env, jobject object, jbyte num_value,
                                                   jbyteArray attr_values, jbyte attr_status) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  if (num_value > 16) {  // MAX ATTR SIZE = 16
    ALOGE("number of attributes exceed maximum");
    return JNI_FALSE;
  }

  std::vector<uint8_t> attrValues;

  jbyte *attr = env->GetByteArrayElements(attr_values, NULL);
  if (!attr) {
    jniThrowIOException(env, EINVAL);
    return JNI_FALSE;
  }

  for (int i = 0; i < num_value; ++i) {
    attrValues.push_back(std::move((uint8_t)attr[i]));
  }

  env->ReleaseByteArrayElements(attr_values, attr, 0);
  player_application_setting_value_cb.Run(num_value, attrValues, attr_status);

  return JNI_TRUE;
}

//JNI Method called to Respond to PDU 0x13
static jboolean sendCurrentPlayerValueRspNative(JNIEnv *env, jobject object, jbyte num_attr,
                                                jbyteArray attr_values, jbyte attr_status) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  if (num_attr > 16 || num_attr == 0) {
    ALOGE("number of attributes exceed maximum or zero");
    return JNI_FALSE;
  }

  std::vector<uint8_t> attrIds;
  std::vector<uint8_t> attrVals;

  jbyte *attr = env->GetByteArrayElements(attr_values, NULL);
  if (!attr) {
    jniThrowIOException(env, EINVAL);
    return JNI_FALSE;
  }

  uint8_t attrNum = num_attr / 2;
  for (int i = 0 ; i < num_attr; i += 2) {
    attrIds.push_back(std::move((uint8_t)attr[i]));
    attrVals.push_back(std::move((uint8_t)attr[i+1]));
  }

  env->ReleaseByteArrayElements(attr_values, attr, 0);
  current_player_setting_value_cb.Run(attrNum, attrIds, attrVals, attr_status);

  return JNI_TRUE;
}

//JNI Method called to Respond to PDU 0x14
static jboolean sendSetPlayerApplicationRspNative(JNIEnv *env, jobject object, jbyte attr_status) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  set_player_application_setting_value_cb.Run((uint8_t) attr_status);

  return JNI_TRUE;  // Shall be always be TRUE??
}

//JNI Method Called to Respond to PDU 0x15
static jboolean sendPlayerApplicationSettingsTextRspNative(JNIEnv *env, jobject object,
                                                           jint num_attr, jbyteArray attr_ids,
                                                           jint text_length,
                                                           jobjectArray attr_text_strings,
                                                           jbyte attr_status) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  if (num_attr > 16) {
    ALOGE("get_element_attr_rsp: number of attributes exceed maximum");
    return JNI_FALSE;
  }

  jbyte *attr = env->GetByteArrayElements(attr_ids, NULL);
  if (!attr) {
    jniThrowIOException(env, EINVAL);
    return JNI_FALSE;
  }

  jstring text;
  const char* textStr;
  std::vector<uint8_t> attIds;
  std::vector<std::string> textStrings;

  for (int i = 0; i < num_attr ; ++i) {
    text = (jstring)env->GetObjectArrayElement(attr_text_strings, i);
    textStr = env->GetStringUTFChars(text, NULL);
    if (!textStr) {
      ALOGE("GetStringUTFChars return NULL");
      env->DeleteLocalRef(text);
      break;
    }

    attIds.push_back(std::move((uint8_t)attr[i]));
    textStrings.push_back(std::string(textStr));

    env->ReleaseStringUTFChars(text, textStr);
    env->DeleteLocalRef(text);
  }

  env->ReleaseByteArrayElements(attr_ids, attr, 0);
  player_application_setting_attribute_text_cb.Run(num_attr, attIds, textStrings, attr_status);

  return JNI_TRUE;
}

//JNI Method Called to respond to PDU 0x16
static jboolean sendPlayerApplicationValueTextRspNative(JNIEnv *env, jobject object, jint num_value,
                                                        jbyteArray attr_ids, jint length,
                                                        jobjectArray value_text_strings,
                                                        jbyte attr_status) {
  ALOGD("%s", __func__);
  std::shared_lock<std::shared_timed_mutex> lock(callbacks_mutex);

  if (num_value > 16) {
    ALOGE("get_element_attr_rsp: number of attributes exceed maximum");
    return JNI_FALSE;
  }

  jbyte *attr = env->GetByteArrayElements(attr_ids, NULL);
  if (!attr) {
    jniThrowIOException(env, EINVAL);
    return JNI_FALSE;
  }

  jstring text;
  const char* textStr;
  std::vector<uint8_t> attIds;
  std::vector<std::string> textStrings;

  for (int i = 0; i < num_value ; ++i) {
    text = (jstring)env->GetObjectArrayElement(value_text_strings, i);
    textStr = env->GetStringUTFChars(text, NULL);
    if (!textStr) {
      ALOGE("GetStringUTFChars return NULL");
      env->DeleteLocalRef(text);
      break;
    }

    attIds.push_back(std::move((uint8_t)attr[i]));
    textStrings.push_back(std::string(textStr));

    env->ReleaseStringUTFChars(text, textStr);
    env->DeleteLocalRef(text);
  }

  env->ReleaseByteArrayElements(attr_ids, attr, 0);
  player_application_setting_value_text_cb.Run(num_value, attIds, textStrings, attr_status);

  return JNI_TRUE;
}

static JNINativeMethod sMethods[] = {
    {"classInitNative", "()V", (void*)classInitNative},
    {"initNative", "()V", (void*)initNative},
    {"registerBipServerNative", "(I)V", (void*)registerBipServerNative},
    {"unregisterBipServerNative", "()V", (void*)unregisterBipServerNative},
    {"sendMediaUpdateNative", "(ZZZ)V", (void*)sendMediaUpdateNative},
    {"sendFolderUpdateNative", "(ZZZ)V", (void*)sendFolderUpdateNative},
    {"sendA2dpStatusChangeNative", "(Ljava/lang/String;Z)V",
     (void*)sendA2dpStatusChangeNative},
    {"sendPlayerApplicationSettingsUpdateNative", "()V",
     (void*)sendPlayerApplicationSettingsUpdateNative},
    {"setAddressedPlayerResponseNative", "(I)V",
     (void*)setAddressedPlayerResponseNative},
    {"setBrowsedPlayerResponseNative", "(IZLjava/lang/String;I)V",
     (void*)setBrowsedPlayerResponseNative},
    {"getFolderItemsResponseNative", "(Ljava/lang/String;Ljava/util/List;)V",
     (void*)getFolderItemsResponseNative},
    {"cleanupNative", "()V", (void*)cleanupNative},
    {"connectDeviceNative", "(Ljava/lang/String;)Z",
     (void*)connectDeviceNative},
    {"disconnectDeviceNative", "(Ljava/lang/String;)Z",
     (void*)disconnectDeviceNative},
    {"sendVolumeChangedNative", "(Ljava/lang/String;I)V",
     (void*)sendVolumeChangedNative},
    {"setBipClientStatusNative", "(Ljava/lang/String;Z)V",
     (void*)setBipClientStatusNative},
    {"getListPlayerApplicationAttrRspNative", "(B[B)Z",
     (void *)getListPlayerApplicationAttrRspNative},
    {"getPlayerApplicationValueRspNative", "(I[BB)Z",
     (void *)getPlayerApplicationValueRspNative},
    {"sendCurrentPlayerValueRspNative", "(I[BB)Z",
     (void *)sendCurrentPlayerValueRspNative},
    {"sendSetPlayerApplicationRspNative", "(B)Z",
     (void *)sendSetPlayerApplicationRspNative},
    {"sendPlayerApplicationSettingsTextRspNative" , "(I[BI[Ljava/lang/String;B)Z",
     (void *)sendPlayerApplicationSettingsTextRspNative},
    {"sendPlayerApplicationValueTextRspNative" , "(I[BI[Ljava/lang/String;B)Z",
     (void *)sendPlayerApplicationValueTextRspNative},
};

int register_com_android_bluetooth_avrcp_target(JNIEnv* env) {
  return jniRegisterNativeMethods(
      env, "com/android/bluetooth/avrcp/AvrcpNativeInterface", sMethods,
      NELEM(sMethods));
}

}  // namespace android
