/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "get_curr_player_app_setting_value.h"

namespace bluetooth {
namespace avrcp {

/**
  * GetCurrentPlayerApplicationSettingValueRequest functions
  */
uint8_t GetCurrentPlayerApplicationSettingValueRequest::GetNumberOfAttributes() const {
  auto it = begin() + VendorPacket::kMinSize();
  return it.extract<uint8_t>();
}

std::vector<uint8_t> GetCurrentPlayerApplicationSettingValueRequest::GetAttributesRequested() const {
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(1);

  size_t number_of_attributes = GetNumberOfAttributes();
  std::vector<uint8_t> attribute_list;

  for (size_t i = 0; i < number_of_attributes; i++) {
    attribute_list.push_back((uint8_t)it.extractBE<uint8_t>());
  }

  return attribute_list;
}

bool GetCurrentPlayerApplicationSettingValueRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;

  if (size() < kMinSize()) return false;

  // -1 to subtract first attribute from kMinSize
  size_t kMaxSize = (kMinSize() - 1) + GetNumberOfAttributes();
  return size() == kMaxSize;
}

std::string GetCurrentPlayerApplicationSettingValueRequest::ToString() const {

  std::stringstream ss;
  ss << ": GetCurrentPlayerApplicationSettingValueRequest " << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Length = " << loghex(GetParameterLength()) << std::endl;
  ss << "  └ Number of Attributes = " << loghex(GetNumberOfAttributes()) << std::endl;
  auto attr_list = GetAttributesRequested();

  ss << "  └ Attribute List: Size: " << attr_list.size() << std::endl;
  for (auto it = attr_list.begin(); it != attr_list.end(); it++) {
    ss << "      └ " << loghex((uint32_t)(*it)) << std::endl;
  }
  ss << std::endl;

  return ss.str();
}


/**
  * GetCurrentPlayerApplicationSettingValueResponseBuilder functions
  */
std::unique_ptr<GetCurrentPlayerApplicationSettingValueResponseBuilder>
    GetCurrentPlayerApplicationSettingValueResponseBuilder::MakeBuilder(
        uint8_t numOfVals, std::vector<uint8_t> attr_ids, std::vector<uint8_t> attrs) {
  std::unique_ptr<GetCurrentPlayerApplicationSettingValueResponseBuilder> builder(
      new GetCurrentPlayerApplicationSettingValueResponseBuilder(numOfVals, attr_ids, attrs));

  return builder;
}

size_t GetCurrentPlayerApplicationSettingValueResponseBuilder::size() const {
  size_t len = VendorPacket::kMinSize();
  /**
   * AVRCP ListPlayerApplicationSettingAttributesResponse:
   *     uint8_t num_of_values;
   *     uint8_t[] attributes;
   *     uint8_t[] values;
   */

  CHECK(attr_ids_.size() == numOfVals_);
  CHECK(attrs_.size() == numOfVals_);

  len += 1;
  len += attr_ids_.size();
  len += attrs_.size();

  return len;
}

bool GetCurrentPlayerApplicationSettingValueResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  AddPayloadOctets1(pkt, numOfVals_);

  for (int i = 0; i < numOfVals_; i++) {
    AddPayloadOctets1(pkt, (uint8_t) (attr_ids_[i]));
    AddPayloadOctets1(pkt, (uint8_t) (attrs_[i]));
  }

  return true;
}

}  // namespace avrcp
}  // namespace bluetooth
