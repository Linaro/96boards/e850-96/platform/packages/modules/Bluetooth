/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "get_player_app_setting_attribute_text.h"

namespace bluetooth {
namespace avrcp {

/**
  * GetPlayerApplicationSettingAttributeTextRequest functions
  */

uint8_t GetPlayerApplicationSettingAttributeTextRequest::GetNumberOfAttributes() const {
  auto it = begin() + VendorPacket::kMinSize();
  return it.extract<uint8_t>();
}

std::vector<uint8_t> GetPlayerApplicationSettingAttributeTextRequest::GetAttributesRequested() const {
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(1);

  size_t number_of_attributes = GetNumberOfAttributes();
  std::vector<uint8_t> attribute_list;

  for (size_t i = 0; i < number_of_attributes; i++) {
    attribute_list.push_back((uint8_t)it.extractBE<uint8_t>());
  }

  return attribute_list;
}

bool GetPlayerApplicationSettingAttributeTextRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;

  // Packet must be larger than smallest possible packet
  if (size() < kMinSize()) return false;

  // -1 to subtract first attribute from kMinSize
  uint8_t kMaxSize = (kMinSize() - 1) + GetNumberOfAttributes();
  return size() == kMaxSize;
}

std::string GetPlayerApplicationSettingAttributeTextRequest::ToString() const {

  std::stringstream ss;
  ss << ": GetPlayerApplicationSettingAttributeTextRequest" << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Length = " << loghex(GetParameterLength()) << std::endl;
  ss << "  └ Number of Attributes = " << loghex(GetNumberOfAttributes()) << std::endl;
  auto attr_list = GetAttributesRequested();

  ss << "  └ Attribute List: Size: " << attr_list.size() << std::endl;
  for (auto it = attr_list.begin(); it != attr_list.end(); it++) {
    ss << "      └ " << loghex((uint8_t)(*it)) << std::endl;
  }
  ss << std::endl;

  return ss.str();
}

/**
  * GetPlayerApplicationSettingAttributeTextResponseBuilder functions
  */
std::unique_ptr<GetPlayerApplicationSettingAttributeTextResponseBuilder>
    GetPlayerApplicationSettingAttributeTextResponseBuilder::MakeBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> setting_attrs, std::vector<std::string> string_attrs) {
  std::unique_ptr<GetPlayerApplicationSettingAttributeTextResponseBuilder> builder(
      new GetPlayerApplicationSettingAttributeTextResponseBuilder(
          numOfAttrs, setting_attrs, string_attrs));

  return builder;
}

size_t GetPlayerApplicationSettingAttributeTextResponseBuilder::size() const {
  size_t len = VendorPacket::kMinSize();
  /**
   * AVRCP GetPlayerApplicationSettingAttributeTextResponse:
   *     uint8_t num_of_attributes;
   *     uint8_t[] setting_attributes;
   *     uint16_t[] character_set_ids;
   *     uint8_t[] string_lengths;
   *     std::string[] attribute_strings;
   */

  CHECK(setting_attrs_.size() == numOfAttrs_);
  CHECK(string_attrs_.size() == numOfAttrs_);

  len += 1;
  len += setting_attrs_.size();
  len += 2 * numOfAttrs_;
  len += string_attrs_.size();
  for (int i = 0; i < numOfAttrs_; i++) {
    size_t str_len = string_attrs_[i].length();
    len += str_len < MAX_SETTING_STRING_LEN ? (uint8_t) str_len : MAX_SETTING_STRING_LEN;
  }

  return len;
}

bool GetPlayerApplicationSettingAttributeTextResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  AddPayloadOctets1(pkt, numOfAttrs_);

  for (int i = 0; i < numOfAttrs_; i++) {
    AddPayloadOctets1(pkt, (uint8_t) (setting_attrs_[i]));
    AddPayloadOctets2(pkt, base::ByteSwap((uint16_t)0x006a));  // UTF-8
    std::string set_str = string_attrs_[i];
    uint8_t str_len = set_str.length() < MAX_SETTING_STRING_LEN ? (uint8_t) set_str.length()
        : MAX_SETTING_STRING_LEN;
    AddPayloadOctets1(pkt, (uint8_t) (str_len));
    for (int j = 0; j < str_len; j++) {
      AddPayloadOctets1(pkt, (uint8_t) (set_str[j]));
    }
  }

  return true;
}

}  // namespace avrcp
}  // namespace bluetooth
