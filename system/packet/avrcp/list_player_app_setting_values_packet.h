/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class ListPlayerApplicationSettingValuesRequest : public VendorPacket {
  public:
    virtual ~ListPlayerApplicationSettingValuesRequest() = default;
    /**
     * AVRCP ListPlayerApplicationSettingAttributesRequest:
     *     uint8_t attribute_id;
     */

    static constexpr size_t kMinSize() { return VendorPacket::kMinSize() + 1; }

    uint8_t GetAttributeID() const;

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

    protected:
      using VendorPacket::VendorPacket;
};

class ListPlayerApplicationSettingValuesResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~ListPlayerApplicationSettingValuesResponseBuilder() = default;

    /**
     * AVRCP ListPlayerApplicationSettingAttributesResponse:
     *     uint8_t num_of_attributes;
     *     uint8_t[] attributes;
     */

    static std::unique_ptr<ListPlayerApplicationSettingValuesResponseBuilder>
        MakeBuilder(uint8_t numOfAttrs, std::vector<uint8_t> attrs);

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  private:
    uint8_t numOfAttrs_;
    std::vector<uint8_t> attrs_;

    ListPlayerApplicationSettingValuesResponseBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> attrs) :
            VendorPacketBuilder(CType::STABLE,
                                CommandPdu::LIST_PLAYER_APPLICATION_SETTING_VALUES,
                                PacketType::SINGLE),
                                numOfAttrs_(numOfAttrs),
                                attrs_(attrs) {};
};

}  // namespace avrcp
}  // namespace bluetooth
