/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class ListApplicationSettingAttributesRequest : public VendorPacket {
  public:
    virtual ~ListApplicationSettingAttributesRequest() = default;

    /**
     * AVRCP ListPlayerApplicationSettingAttributesRequest:
     *     None
     */
    static constexpr size_t kMinSize() {
      return VendorPacket::kMinSize();
    }

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

  protected:
    using VendorPacket::VendorPacket;
};

class ListApplicationSettingAttributesResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~ListApplicationSettingAttributesResponseBuilder() = default;

    /**
     * AVRCP ListPlayerApplicationSettingAttributesResponse:
     *     uint8_t num_of_attributes;
     *     uint8_t[] attributes;
     */
    static std::unique_ptr<ListApplicationSettingAttributesResponseBuilder>
        MakeBuilder(uint8_t numOfAttrs, std::vector<uint8_t> attrs);

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  protected:
    uint8_t numOfAttrs_;
    std::vector<uint8_t> attrs_;

    ListApplicationSettingAttributesResponseBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> attrs) :
        VendorPacketBuilder(CType::STABLE,
                            CommandPdu::LIST_PLAYER_APPLICATION_SETTING_ATTRIBUTES,
                            PacketType::SINGLE),
                            numOfAttrs_(numOfAttrs),
                            attrs_(attrs) {};
};
}  // namespace avrcp
}  // namespace bluetooth
