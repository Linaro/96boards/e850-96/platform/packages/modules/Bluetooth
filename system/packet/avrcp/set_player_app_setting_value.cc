/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "set_player_app_setting_value.h"

namespace bluetooth {
namespace avrcp {

/**
  * SetPlayerApplicationSettingValueRequest functions
  */

uint8_t SetPlayerApplicationSettingValueRequest::GetNumberOfAttributes() const {
  auto it = begin() + VendorPacket::kMinSize();
  return it.extract<uint8_t>();
}

std::vector<uint8_t> SetPlayerApplicationSettingValueRequest::GetAttributesSet() const {
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(1);

  size_t number_of_attributes = GetNumberOfAttributes();
  std::vector<uint8_t> attribute_ids;

  for (size_t i = 0; i < number_of_attributes; i++) {
    attribute_ids.push_back((uint8_t)it.extractBE<uint8_t>());
  }

  return attribute_ids;
}

std::vector<uint8_t> SetPlayerApplicationSettingValueRequest::GetAttributesValues() const {
  size_t number_of_attributes = GetNumberOfAttributes();
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(1) +
      static_cast<size_t>(number_of_attributes);

  std::vector<uint8_t> attribute_values;

  for (size_t i = 0; i < number_of_attributes; i++) {
    attribute_values.push_back((uint8_t)it.extractBE<uint8_t>());
  }

  return attribute_values;
}

bool SetPlayerApplicationSettingValueRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;

  if (size() < kMinSize()) return false;

  // -1 to subtract first attribute from kMinSize
  uint8_t kMaxSize = (kMinSize() - 1) + GetNumberOfAttributes();
  return size() == kMaxSize;
}

std::string SetPlayerApplicationSettingValueRequest::ToString() const {
  std::stringstream ss;
  ss << ": SetPlayerApplicationSettingValueRequest" << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Length = " << loghex(GetParameterLength()) << std::endl;
  ss << "  └ Number of Attributes = " << loghex(GetNumberOfAttributes()) << std::endl;
  auto attr_list = GetAttributesSet();

  ss << "  └ Attributes Set: Size: " << attr_list.size() << std::endl;
  for (auto it = attr_list.begin(); it != attr_list.end(); it++) {
    ss << "      └ " << loghex((uint32_t)(*it)) << std::endl;
  }

  auto attr_vals = GetAttributesValues();
  ss << "  └ Attributes Values: Size: " << attr_vals.size() << std::endl;
  for (auto it = attr_vals.begin(); it != attr_vals.end(); it++) {
    ss << "      └ " << loghex((uint32_t)(*it)) << std::endl;
  }
  ss << std::endl;

  return ss.str();
}


std::unique_ptr<SetPlayerApplicationSettingValueResponseBuilder>
    SetPlayerApplicationSettingValueResponseBuilder::MakeBuilder() {
  std::unique_ptr<SetPlayerApplicationSettingValueResponseBuilder> builder(
      new SetPlayerApplicationSettingValueResponseBuilder());

  return builder;
}

size_t SetPlayerApplicationSettingValueResponseBuilder::size() const {
  return VendorPacket::kMinSize();
}

bool SetPlayerApplicationSettingValueResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  return true;
}


}  // namespace avrcp
}  // namespace bluetooth
