/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class GetPlayerApplicationSettingAttributeTextRequest : public VendorPacket {
  public:
    virtual ~GetPlayerApplicationSettingAttributeTextRequest() = default;
    /**
     * AVRCP GetPlayerApplicationSettingAttributeTextRequest:
     *     uint8_t attribute_id;
     *     uint8_t[] attributes;
     */

    static constexpr size_t kMinSize() { return VendorPacket::kMinSize() + 2; }

    uint8_t GetNumberOfAttributes() const;
    std::vector<uint8_t> GetAttributesRequested() const;

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

    protected:
      using VendorPacket::VendorPacket;
};

class GetPlayerApplicationSettingAttributeTextResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~GetPlayerApplicationSettingAttributeTextResponseBuilder() = default;

    /**
     * AVRCP GetPlayerApplicationSettingAttributeTextResponse:
     *     uint8_t num_of_attributes;
     *     uint8_t[] setting_attributes;
     *     uint16_t[] character_set_ids;
     *     uint8_t[] string_lengths;
     *     std::string[] attribute_strings;
     */

    static std::unique_ptr<GetPlayerApplicationSettingAttributeTextResponseBuilder>
        MakeBuilder(uint8_t numOfAttrs, std::vector<uint8_t> setting_attrs,
                    std::vector<std::string> string_attrs);

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  private:
    uint8_t numOfAttrs_;
    std::vector<uint8_t> setting_attrs_;
    std::vector<std::string> string_attrs_;

    GetPlayerApplicationSettingAttributeTextResponseBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> setting_attrs,
        std::vector<std::string> string_attrs) :
            VendorPacketBuilder(CType::STABLE,
                                CommandPdu::GET_PLAYER_APPLICATION_SETTING_ATTRIBUTE_TEXT,
                                PacketType::SINGLE),
                                numOfAttrs_(numOfAttrs),
                                setting_attrs_(setting_attrs),
                                string_attrs_(string_attrs) {};
};

}  // namespace avrcp
}  // namespace bluetooth
