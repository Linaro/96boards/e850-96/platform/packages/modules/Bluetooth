/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class GetCurrentPlayerApplicationSettingValueRequest : public VendorPacket {
  public:
    virtual ~GetCurrentPlayerApplicationSettingValueRequest() = default;
    /**
     * AVRCP GetCurrentPlayerApplicationSettingValueRequest:
     *     uint8_t attribute_id;
     *     uint8_t[] attributes;
     */

    static constexpr size_t kMinSize() { return VendorPacket::kMinSize() + 2; }

    uint8_t GetNumberOfAttributes() const;
    std::vector<uint8_t> GetAttributesRequested() const;

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

    protected:
      using VendorPacket::VendorPacket;
};

class GetCurrentPlayerApplicationSettingValueResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~GetCurrentPlayerApplicationSettingValueResponseBuilder() = default;

    /**
     * AVRCP GetCurrentPlayerApplicationSettingValueResponse:
     *     uint8_t num_of_values;
     *     uint8_t[] attributes;
     *     uint8_t[] values;
     */

    static std::unique_ptr<GetCurrentPlayerApplicationSettingValueResponseBuilder>
        MakeBuilder(uint8_t numOfVals, std::vector<uint8_t> attr_ids,
                    std::vector<uint8_t> attrs);

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  private:
    uint8_t numOfVals_;
    std::vector<uint8_t> attr_ids_;
    std::vector<uint8_t> attrs_;

    GetCurrentPlayerApplicationSettingValueResponseBuilder(
        uint8_t numOfVals, std::vector<uint8_t> attr_ids, std::vector<uint8_t> attrs) :
            VendorPacketBuilder(CType::STABLE,
                                CommandPdu::GET_CURRENT_PLAYER_APPLICATION_SETTING_VALUE,
                                PacketType::SINGLE),
                                numOfVals_(numOfVals),
                                attr_ids_(attr_ids),
                                attrs_(attrs) {};
};

}  // namespace avrcp
}  // namespace bluetooth
