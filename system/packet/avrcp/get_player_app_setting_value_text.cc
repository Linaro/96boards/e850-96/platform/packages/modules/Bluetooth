/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "get_player_app_setting_value_text.h"

namespace bluetooth {
namespace avrcp {

/**
  * GetPlayerApplicationSettingValueTextRequest functions
  */

uint8_t GetPlayerApplicationSettingValueTextRequest::GetAttributeID() const {
  auto it = begin() + VendorPacket::kMinSize();
  return it.extract<uint8_t>();
}

uint8_t GetPlayerApplicationSettingValueTextRequest::GetNumberOfSettingValues() const {
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(1);
  return it.extract<uint8_t>();
}

std::vector<uint8_t> GetPlayerApplicationSettingValueTextRequest::GetSettingIDsRequested() const {
  auto it = begin() + VendorPacket::kMinSize() + static_cast<size_t>(2);

  size_t number_of_attributes = GetNumberOfSettingValues();
  std::vector<uint8_t> value_ids;

  for (size_t i = 0; i < number_of_attributes; i++) {
    value_ids.push_back((uint8_t)it.extractBE<uint8_t>());
  }

  return value_ids;
}

bool GetPlayerApplicationSettingValueTextRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;

  // Packet must be larger than smallest possible packet
  if (size() < kMinSize()) return false;

  // -1 to subtract first attribute from kMinSize
  uint8_t kMaxSize = (kMinSize() - 1) + GetNumberOfSettingValues();
  return size() == kMaxSize;
}

std::string GetPlayerApplicationSettingValueTextRequest::ToString() const {

  std::stringstream ss;
  ss << ": GetPlayerApplicationSettingValueTextRequest" << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Length = " << loghex(GetParameterLength()) << std::endl;
  ss << "  └ Attribute ID = " << loghex(GetAttributeID()) << std::endl;
  ss << "  └ Number of Setting Values= " << loghex(GetNumberOfSettingValues()) << std::endl;
  auto attr_list = GetSettingIDsRequested();

  ss << "  └ Setting Value IDs: Size: " << attr_list.size() << std::endl;
  for (auto it = attr_list.begin(); it != attr_list.end(); it++) {
    ss << "      └ " << loghex((uint8_t)(*it)) << std::endl;
  }
  ss << std::endl;

  return ss.str();
}


/**
  * GetPlayerApplicationSettingValueTextResponseBuilder functions
  */
std::unique_ptr<GetPlayerApplicationSettingValueTextResponseBuilder>
    GetPlayerApplicationSettingValueTextResponseBuilder::MakeBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> setting_vals_ids, std::vector<std::string> string_attrs) {
  std::unique_ptr<GetPlayerApplicationSettingValueTextResponseBuilder> builder(
      new GetPlayerApplicationSettingValueTextResponseBuilder(
          numOfAttrs, setting_vals_ids, string_attrs));

  return builder;
}

size_t GetPlayerApplicationSettingValueTextResponseBuilder::size() const {
  size_t len = VendorPacket::kMinSize();
  /**
   * AVRCP GetPlayerApplicationSettingValueTextResponse:
   *     uint8_t num_of_attributes;
   *     uint8_t[] setting_values;
   *     uint16_t[] character_set_ids;
   *     uint8_t[] string_lengths;
   *     std::string[] value_strings;
   */

  CHECK(setting_vals_ids_.size() == numOfAttrs_);
  CHECK(string_attrs_.size() == numOfAttrs_);

  len += 1;
  len += setting_vals_ids_.size();
  len += 2 * numOfAttrs_;
  len += string_attrs_.size();
  for (int i = 0; i < numOfAttrs_; i++) {
    size_t str_len = string_attrs_[i].length();
    len += str_len < MAX_SETTING_STRING_LEN ? (uint8_t) str_len : MAX_SETTING_STRING_LEN;
  }

  return len;
}

bool GetPlayerApplicationSettingValueTextResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  AddPayloadOctets1(pkt, numOfAttrs_);

  for (int i = 0; i < numOfAttrs_; i++) {
    AddPayloadOctets1(pkt, (uint8_t) (setting_vals_ids_[i]));
    AddPayloadOctets2(pkt, base::ByteSwap((uint16_t)0x006a));  // UTF-8
    std::string set_str = string_attrs_[i];

    uint8_t str_len = set_str.length() < MAX_SETTING_STRING_LEN ? (uint8_t) set_str.length()
        : MAX_SETTING_STRING_LEN;
    AddPayloadOctets1(pkt, (uint8_t) (str_len));

    for (int j = 0; j < str_len; j++) {
      AddPayloadOctets1(pkt, (uint8_t) (set_str[j]));
    }
  }

  return true;
}

}  // namespace avrcp
}  // namespace bluetooth
