/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class SetPlayerApplicationSettingValueRequest : public VendorPacket {
  public:
    virtual ~SetPlayerApplicationSettingValueRequest() = default;
    /**
     * AVRCP SetPlayerApplicationSettingValueRequest:
     *     uint8_t num_of_attributes;
     *     uint8_t[] attributes;
     *     uint8_t[] values;
     */

    static constexpr size_t kMinSize() { return VendorPacket::kMinSize() + 3; }

    uint8_t GetNumberOfAttributes() const;
    std::vector<uint8_t> GetAttributesSet() const;
    std::vector<uint8_t> GetAttributesValues() const;

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

    protected:
      using VendorPacket::VendorPacket;
};

class SetPlayerApplicationSettingValueResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~SetPlayerApplicationSettingValueResponseBuilder() = default;

    /**
     * AVRCP SetPlayerApplicationSettingValueResponse:
     *    None
     */

    static std::unique_ptr<SetPlayerApplicationSettingValueResponseBuilder>
        MakeBuilder();

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  private:

    SetPlayerApplicationSettingValueResponseBuilder() :
            VendorPacketBuilder(CType::ACCEPTED,
                                CommandPdu::SET_PLAYER_APPLICATION_SETTING_VALUE,
                                PacketType::SINGLE) {};
};

}  // namespace avrcp
}  // namespace bluetooth
