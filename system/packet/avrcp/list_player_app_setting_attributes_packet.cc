/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "list_player_app_setting_attributes_packet.h"

namespace bluetooth {
namespace avrcp {

/*
 * ListApplicationSettingAttributesRequest functions
 */

bool ListApplicationSettingAttributesRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;
  return size() == kMinSize();
}

std::string ListApplicationSettingAttributesRequest::ToString() const {
  std::stringstream ss;
  ss << "ListApplicationSettingAttributesRequest: " << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Parameter Length = " << loghex(GetParameterLength()) << std::endl;
  ss << std::endl;

  return ss.str();
}

/*
 * ListApplicationSettingAttributesResponseBuilder functions
 */

std::unique_ptr<ListApplicationSettingAttributesResponseBuilder>
    ListApplicationSettingAttributesResponseBuilder::MakeBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> attrs) {
  std::unique_ptr<ListApplicationSettingAttributesResponseBuilder> builder(
      new ListApplicationSettingAttributesResponseBuilder(numOfAttrs, attrs));

  return builder;
}

size_t ListApplicationSettingAttributesResponseBuilder::size() const {
  /**
   * AVRCP ListPlayerApplicationSettingAttributesResponse:
   *     uint8_t num_of_attributes;
   *     uint8_t[] attributes;
   */

  CHECK(attrs_.size() == numOfAttrs_);

  size_t len = VendorPacket::kMinSize();
  len += 1;
  len += attrs_.size();
  return len;
}

bool ListApplicationSettingAttributesResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  AddPayloadOctets1(pkt, numOfAttrs_);
  for (int i = 0; i < numOfAttrs_; i++) {
    AddPayloadOctets1(pkt, (uint8_t) (attrs_[i]));
  }
  return true;
}

}  // namespace avrcp
}  // namespace bluetooth
