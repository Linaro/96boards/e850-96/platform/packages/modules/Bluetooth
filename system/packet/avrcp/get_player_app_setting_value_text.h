/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#pragma once

#include "vendor_packet.h"

namespace bluetooth {
namespace avrcp {

class GetPlayerApplicationSettingValueTextRequest : public VendorPacket {
  public:
    virtual ~GetPlayerApplicationSettingValueTextRequest() = default;
    /**
     * AVRCP GetPlayerApplicationSettingValueTextRequest:
     *     uint8_t attribute_id;
     *     uint8_t num_of_values;
     *     uint8_t[] setting_ids;
     */

    static constexpr size_t kMinSize() { return VendorPacket::kMinSize() + 3; }

    uint8_t GetAttributeID() const;
    uint8_t GetNumberOfSettingValues() const;
    std::vector<uint8_t> GetSettingIDsRequested() const;

    virtual bool IsValid() const override;
    virtual std::string ToString() const override;

    protected:
      using VendorPacket::VendorPacket;
};

class GetPlayerApplicationSettingValueTextResponseBuilder : public VendorPacketBuilder {
  public:
    virtual ~GetPlayerApplicationSettingValueTextResponseBuilder() = default;

    /**
     * AVRCP GetPlayerApplicationSettingValueTextResponse:
     *     uint8_t num_of_attributes;
     *     uint8_t[] setting_values;
     *     uint16_t[] character_set_ids;
     *     uint8_t[] string_lengths;
     *     std::string[] value_strings;
     */

    static std::unique_ptr<GetPlayerApplicationSettingValueTextResponseBuilder>
        MakeBuilder(uint8_t numOfAttrs,
                    std::vector<uint8_t> setting_vals_ids,
                    std::vector<std::string> attr_values);

    virtual size_t size() const override;
    virtual bool Serialize(const std::shared_ptr<::bluetooth::Packet> &pkt) override;

  private:
    uint8_t numOfAttrs_;
    std::vector<uint8_t> setting_vals_ids_;
    std::vector<std::string> string_attrs_;

    GetPlayerApplicationSettingValueTextResponseBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> setting_vals_ids, std::vector<std::string> attr_values) :
            VendorPacketBuilder(CType::STABLE,
                                CommandPdu::GET_PLAYER_APPLICATION_SETTING_VALUE_TEXT,
                                PacketType::SINGLE),
                                numOfAttrs_(numOfAttrs),
                                setting_vals_ids_(setting_vals_ids),
                                string_attrs_(attr_values) {};
};

}  // namespace avrcp
}  // namespace bluetooth
