/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#include "list_player_app_setting_values_packet.h"

namespace bluetooth {
namespace avrcp {

/**
  * ListPlayerApplicationSettingValuesRequest functions
  */

uint8_t ListPlayerApplicationSettingValuesRequest::GetAttributeID() const {
  auto it = begin() + VendorPacket::kMinSize();
  return it.extract<uint8_t>();
}

bool ListPlayerApplicationSettingValuesRequest::IsValid() const {
  if (!VendorPacket::IsValid()) return false;
  return size() == kMinSize();
}

std::string ListPlayerApplicationSettingValuesRequest::ToString() const {

  std::stringstream ss;
  ss << "ListPlayerApplicationSettingValuesRequest: " << std::endl;
  ss << "  └ cType = " << GetCType() << std::endl;
  ss << "  └ Subunit Type = " << loghex(GetSubunitType()) << std::endl;
  ss << "  └ Subunit ID = " << loghex(GetSubunitId()) << std::endl;
  ss << "  └ OpCode = " << GetOpcode() << std::endl;
  ss << "  └ Company ID = " << loghex(GetCompanyId()) << std::endl;
  ss << "  └ Command PDU = " << GetCommandPdu() << std::endl;
  ss << "  └ PacketType = " << GetPacketType() << std::endl;
  ss << "  └ Length = " << loghex(GetParameterLength()) << std::endl;
  ss << "  └ Attribute ID = " << loghex(GetAttributeID()) << std::endl;
  ss << std::endl;

  return ss.str();
}


/**
  * ListPlayerApplicationSettingValuesResponseBuilder functions
  */

std::unique_ptr<ListPlayerApplicationSettingValuesResponseBuilder>
    ListPlayerApplicationSettingValuesResponseBuilder::MakeBuilder(
        uint8_t numOfAttrs, std::vector<uint8_t> attrs) {
  std::unique_ptr<ListPlayerApplicationSettingValuesResponseBuilder> builder(
      new ListPlayerApplicationSettingValuesResponseBuilder(numOfAttrs, attrs));

  return builder;
}

size_t ListPlayerApplicationSettingValuesResponseBuilder::size() const {
  size_t len = VendorPacket::kMinSize();
  /**
   * AVRCP ListPlayerApplicationSettingAttributesResponse:
   *     uint8_t num_of_attributes;
   *     uint8_t[] attributes;
   */

  CHECK(attrs_.size() == numOfAttrs_);

  len += 1;
  len += attrs_.size();

  return len;
}

bool ListPlayerApplicationSettingValuesResponseBuilder::Serialize(
    const std::shared_ptr<::bluetooth::Packet> &pkt) {
  ReserveSpace(pkt, size());

  PacketBuilder::PushHeader(pkt);

  VendorPacketBuilder::PushHeader(pkt, size() - VendorPacket::kMinSize());

  AddPayloadOctets1(pkt, numOfAttrs_);

  for (int i = 0; i < numOfAttrs_; i++) {
    AddPayloadOctets1(pkt, (uint8_t) (attrs_[i]));
  }

  return true;
}

}  // namespace avrcp
}  // namespace bluetooth
