/******************************************************************************
 *
 *  Copyright 2015 Google, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/

#pragma once

#include "device/include/interop.h"
#include "raw_address.h"

typedef struct {
  RawAddress addr;
  size_t length;
  interop_feature_t feature;
} interop_addr_entry_t;

static const interop_addr_entry_t interop_addr_database[] = {
    // Nexus Remote (Spike)
    // Note: May affect other Asus brand devices
    {{{0x08, 0x62, 0x66, 0, 0, 0}}, 3, INTEROP_DISABLE_LE_SECURE_CONNECTIONS},
    {{{0x38, 0x2c, 0x4a, 0xc9, 0, 0}},
     4,
     INTEROP_DISABLE_LE_SECURE_CONNECTIONS},
    {{{0x38, 0x2c, 0x4a, 0xe6, 0, 0}},
     4,
     INTEROP_DISABLE_LE_SECURE_CONNECTIONS},
    {{{0x54, 0xa0, 0x50, 0xd9, 0, 0}},
     4,
     INTEROP_DISABLE_LE_SECURE_CONNECTIONS},
    {{{0xac, 0x9e, 0x17, 0, 0, 0}}, 3, INTEROP_DISABLE_LE_SECURE_CONNECTIONS},
    {{{0xf0, 0x79, 0x59, 0, 0, 0}}, 3, INTEROP_DISABLE_LE_SECURE_CONNECTIONS},

    // Unknown LE device that do not handle secure mode well.
    {{{0x80, 0xea, 0xca, 0, 0, 0}}, 3, INTEROP_DISABLE_LE_SECURE_CONNECTIONS},

    {{{0x08, 0x62, 0x66, 0, 0, 0}}, 3, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},
    {{{0x38, 0x2c, 0x4a, 0xc9, 0, 0}}, 4, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},
    {{{0x38, 0x2c, 0x4a, 0xe6, 0, 0}}, 4, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},
    {{{0x54, 0xa0, 0x50, 0xd9, 0, 0}}, 4, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},
    {{{0xac, 0x9e, 0x17, 0, 0, 0}}, 3, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},
    {{{0xf0, 0x79, 0x59, 0, 0, 0}}, 3, INTEROP_HID_PREF_CONN_SUP_TIMEOUT_3S},

    // Ausdom M05 - unacceptably loud volume
    {{{0xa0, 0xe9, 0xdb, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // BMW car kits (Harman/Becker)
    {{{0x9c, 0xdf, 0x03, 0, 0, 0}}, 3, INTEROP_AUTO_RETRY_PAIRING},

    // Flic smart button
    {{{0x80, 0xe4, 0xda, 0x70, 0, 0}},
     4,
     INTEROP_DISABLE_LE_SECURE_CONNECTIONS},

    // iKross IKBT83B HS - unacceptably loud volume
    {{{0x00, 0x14, 0x02, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // JayBird BlueBuds X - low granularity on volume control
    {{{0x44, 0x5e, 0xf3, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},
    {{{0xd4, 0x9c, 0x28, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Unknown devices that have volume issue.
    {{{0x1c, 0x48, 0xf9, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},
    {{{0x04, 0xf8, 0xc2, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Alphine carkit
    {{{0x48, 0xf0, 0x7b, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // Bose QuiteComfort 35, SoundSport and similar (because of older firmware)
    {{{0x04, 0x52, 0xc7, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // JayBird Family
    {{{0x00, 0x18, 0x91, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // Sony MBH-10
    {{{0x20, 0x15, 0x06, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // Uconnect
    {{{0x00, 0x54, 0xaf, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},
    {{{0x30, 0x14, 0x4a, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // Ford SYNC
    {{{0x50, 0x65, 0x83, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // Mercedes Benz Carkit
    {{{0x00, 0x21, 0x4f, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // HAVAL H6
    {{{0x04, 0xf8, 0xc2, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    //BMW X6 2021
    {{{0xb8, 0x9f, 0x09, 0, 0, 0}}, 3, INTEROP_2MBPS_LINK_ONLY},

    // LG Tone HBS-730 - unacceptably loud volume
    {{{0x00, 0x18, 0x6b, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},
    {{{0xb8, 0xad, 0x3e, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // LG Tone HV-800 - unacceptably loud volume
    {{{0xa0, 0xe9, 0xdb, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Motorola Key Link
    {{{0x1c, 0x96, 0x5a, 0, 0, 0}}, 3, INTEROP_DISABLE_LE_SECURE_CONNECTIONS},

    // Motorola Roadster
    {{{0x00, 0x24, 0x1C, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Mpow Cheetah - unacceptably loud volume
    {{{0x00, 0x11, 0xb1, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Nissan car kits (ALPS) - auto-pairing fails and rejects next pairing
    {{{0x34, 0xc7, 0x31, 0, 0, 0}}, 3, INTEROP_DISABLE_AUTO_PAIRING},

    // SOL REPUBLIC Tracks Air - unable to adjust volume back off from max
    {{{0xa4, 0x15, 0x66, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Subaru car kits (ALPS) - auto-pairing fails and rejects next pairing
    {{{0x00, 0x07, 0x04, 0, 0, 0}}, 3, INTEROP_DISABLE_AUTO_PAIRING},
    {{{0xe0, 0x75, 0x0a, 0, 0, 0}}, 3, INTEROP_DISABLE_AUTO_PAIRING},

    // Swage Rokitboost HS - unacceptably loud volume
    {{{0x00, 0x14, 0xf1, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // VW Car Kit - not enough granularity with volume
    {{{0x00, 0x26, 0x7e, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},
    {{{0x90, 0x03, 0xb7, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // VW Car Kit - Does not change volume correctly
    {{{0x74, 0x6F, 0xF7, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Unknown keyboard (carried over from auto_pair_devlist.conf)
    {{{0x00, 0x0F, 0xF6, 0, 0, 0}}, 3, INTEROP_KEYBOARD_REQUIRES_FIXED_PIN},

    // Kenwood KMM-BT518HD - no audio when A2DP codec sample rate is changed
    {{{0x00, 0x1d, 0x86, 0, 0, 0}}, 3, INTEROP_DISABLE_AVDTP_RECONFIGURE},

    // NAC FORD-2013 - Lincoln
    {{{0x00, 0x26, 0xb4, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Toyota Prius - 2015
    {{{0xfc, 0xc2, 0xde, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Toyota Prius - b/231092023
    {{{0x9c, 0xdf, 0x03, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // OBU II Bluetooth dongle
    {{{0x00, 0x04, 0x3e, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Visteon carkit
    {{{0x00, 0x0a, 0x30, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Roman R9020
    {{{0x00, 0x23, 0x01, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Jabra Storm
    {{{0x1c, 0x48, 0xf9, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Jeep Uconnect
    {{{0x00, 0x54, 0xaf, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // Honda Crider 2019
    {{{0x30, 0xc3, 0xd9, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    //vw carkit
    {{{0x48, 0xa9, 0xd2, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    //Audi A6L
    {{{0x10, 0x98, 0xc3, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH},

    // deepblue2 - cannot change smoothly the volume: b/37834035
    {{{0x0c, 0xa6, 0x94, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // AirPods 2 - unacceptably loud volume
    {{{0x94, 0x16, 0x25, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // AirPods 2 - unacceptably loud volume
    {{{0x9c, 0x64, 0x8b, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Phonak AG - volume level not change
    {{{0x00, 0x0f, 0x59, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Mazda
    {{{0x88, 0x33, 0x14, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // for skip name request,
    // because BR/EDR address and ADV random address are the same
    {{{0xd4, 0x7a, 0xe2, 0, 0, 0}}, 3, INTEROP_DISABLE_NAME_REQUEST},

    // SUBARU Carkit
    {{{0x48, 0xf0, 0x7b, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Audi Carkit
    {{{0x90, 0x03, 0xb7, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},
    {{{0x28, 0xA1, 0x83, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Lexus Carkit
    {{{0x64, 0xd4, 0xbd, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Mazda Carkit
    {{{0xfc, 0x35, 0xe6, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Toyota Car Audio
    {{{0x00, 0x17, 0x53, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Honda High End Carkit
    {{{0x9c, 0x8d, 0x7c, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Honda Civic Carkit
    {{{0x0c, 0xd9, 0xc1, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // BMW Carkit
    {{{0x9c, 0xdf, 0x03, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    //ZOTYE
    {{{0x04, 0xf8, 0xc2, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    //Porsche BT 4594
    {{{0x44, 0x91, 0x60, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    //Haval DarGO 2021 0x48-eb-62-2f-8e-23
    {{{0x48, 0xeb, 0x62, 0, 0, 0}}, 3, INTEROP_AVRCP_1_4_ONLY},

    // Discovery Carkit
    {{{0x00, 0x00, 0x14, 0, 0, 0}}, 3, INTEROP_MAP_1_2_ONLY},

    // KDDI Carkit
    {{{0x44, 0xea, 0xd8, 0, 0, 0}}, 3, INTEROP_DISABLE_SNIFF},

    // Toyota Camry 2018 Carkit HFP AT+BIND missing
    {{{0x94, 0xb2, 0xcc, 0x30, 0, 0}}, 4, INTEROP_SLC_SKIP_BIND_COMMAND},

    // BMW Carkit
    {{{0x00, 0x0a, 0x08, 0, 0, 0}}, 3, INTEROP_AVRCP_1_3_ONLY},

    // BMW X7 2019
    {{{0x2C, 0xDC, 0xAD, 0, 0, 0}}, 3, INTEROP_AVRCP_1_3_ONLY},

    // LG OM4560
    {{{0x08, 0xef, 0x3b, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // SONY SRS-XB2
    {{{0xb8, 0x69, 0xc2, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // JBL Charge 3
    {{{0xb8, 0xd5, 0x0b, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // Alpine Carkit
    {{{0x48, 0xf0, 0x7b, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // Plantronics VOYAGER 5200
    {{{0xe4, 0x22, 0xa5, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // BMW MINI Carkit
    {{{0xb8, 0x24, 0x10, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    //TOYOTA RAV4
    {{{0xfc, 0x62, 0xb9, 0, 0, 0}}, 3, INTEROP_MASTER_ROLE_ONLY},

    // AirPods 2 - unacceptably loud volume
    {{{0x94, 0x16, 0x25, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // AirPods 2 - unacceptably loud volume
    {{{0x9c, 0x64, 0x8b, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // AirPods 2 - unacceptably loud volume
    {{{0x14, 0x60, 0xcb, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // AirPods - unacceptably loud volume
    {{{0x7c, 0x04, 0xd0, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Porsche Car Kit - Does not change volume correctly
    {{{0x48, 0xA9, 0xD2, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Porsche Cayenne
    {{{0x44, 0x91, 0x60, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // JBL Go
    {{{0x78, 0x44, 0x05, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // Ford SYNC
    {{{0xA8, 0x1B, 0x6A, 0, 0, 0}}, 3, INTEROP_DISABLE_ABSOLUTE_VOLUME},

    // BUICK
    {{{0x00, 0x17, 0x53, 0, 0, 0}}, 3, INTEROP_SCO_OPEN_MORE_DELAY},

    // VW Radio
    {{{0x88, 0xbd, 0x78, 0, 0, 0}}, 3, INTEROP_SCO_OPEN_MORE_DELAY},

    // VW Zetta
    {{{0x04, 0xf8, 0xc2, 0, 0, 0}}, 3, INTEROP_SCO_OPEN_MORE_DELAY},

    //Toyotta corolla carkit, issue number is SOC-80440
    {{{0x0d, 0x18, 0x44, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH_IN_LINK_POLICY_SETTINGS},

    //Wulinghongguang carkit, issue number is SOC-71960
    //Nissan carkit, name is BC8-Android, issue number is SOC-81489
    {{{0x00, 0x0d, 0x18, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH_IN_LINK_POLICY_SETTINGS},

    //Toyota prado carkit, issue number is SOC-118913
    {{{0x2c, 0xab, 0x33, 0, 0, 0}}, 3, INTEROP_DISABLE_ROLE_SWITCH_IN_LINK_POLICY_SETTINGS},

    // VW Santana
    {{{0x00, 0x09, 0x93, 0, 0, 0}}, 3, INTEROP_DISABLE_PLAYER_APP_SETTING},
    //{{{0x74, 0x6f, 0xf7, 0, 0, 0}}, 3, INTEROP_DISABLE_PLAYER_APP_SETTING},
    //{{{0xa0, 0x56, 0xb2, 0x4f, 0, 0}}, 4, INTEROP_DISABLE_PLAYER_APP_SETTING},
    //{{{0x00, 0x54, 0xaf, 0, 0, 0}}, 3, INTEROP_DISABLE_PLAYER_APP_SETTING},
    //{{{0x00, 0x00, 0x2e, 0, 0, 0}}, 3, INTEROP_DISABLE_PLAYER_APP_SETTING},
    //{{{0xa0, 0x56, 0xb2, 0x5a, 0, 0}}, 4, INTEROP_DISABLE_PLAYER_APP_SETTING},

    // VW PHONE
    {{{0x90, 0x03, 0xb7, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Audi MIB Standard
    {{{0x34, 0xc7, 0x31, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Audi MIB High
    {{{0x00, 0x17, 0xca, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Acura ZDX 2013
    {{{0x00, 0x0a, 0x30, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // BMW 57523 COMBOX
    {{{0x00, 0x0e, 0x9f, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // SYNC
    {{{0x00, 0x21, 0xcc, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Ford SYNC Touch
    {{{0x00, 0x26, 0xb4, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Chevy Silverado
    {{{0x38, 0xc0, 0x96, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Volkswagen UHV Premium
    {{{0x00, 0x23, 0x3d, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // CADILLAC CUE
    {{{0x04, 0x98, 0xf3, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // FIAT VP4 (Chrysler)
    {{{0x30, 0x14, 0x4a, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Volkswagen Touareg
    {{{0x9c, 0xdf, 0x03, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Seat BT AC
    {{{0xe0, 0x75, 0x0a, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // My Car (Nissan)
    {{{0x00, 0x09, 0x93, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // MY10 Mitsu MDG(Hands Free System)
    {{{0x51, 0x30, 0x26, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // VW MIB DELPHI(VW BT ----)
    {{{0xfc, 0x62, 0xb9, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Audi02
    {{{0xac, 0x7a, 0x4d, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // FIAT VP2 (Uconnect)
    {{{0x9c, 0x28, 0xbf, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Hands Free System
    {{{0x0c, 0xd9, 0xc1, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Audi MMI 4339
    {{{0x48, 0xa9, 0xd2, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Porsche01
    {{{0x60, 0xf1, 0x89, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Chevrolet MyLink
    {{{0x9c, 0x8d, 0x7c, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Volkswagen MIB2 STD
    {{{0x30, 0xc3, 0xd9, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Porsche BT 2110
    {{{0xb8, 0xd7, 0xaf, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Chrysler BT VP2 C
    {{{0x00, 0x54, 0xaf, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // VOLKSWAGEN JETTA
    {{{0x74, 0x95, 0xec, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Ford Focus
    {{{0x88, 0x4a, 0xea, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Ford Fiesta
    {{{0x98, 0x5d, 0xad, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // MINI
    {{{0xb8, 0x24, 0x10, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // CAR MULTIMEDIA
    {{{0xa4, 0x04, 0x50, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // LEXUS
    {{{0x74, 0xd7, 0xca, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // ZOTYE T600 2017
    {{{0x04, 0xf8, 0xc2, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},
    // Mazida CX4 2020
    {{{0x00, 0x0d, 0x18, 0, 0, 0}}, 3, INTEROP_ENABLE_CALL_TERMINATED_AUTOPLAY},

    //Changan Suzuki S-cross 2017
    {{{0x00, 0x17, 0x53, 0, 0, 0}}, 3, INTEROP_DISABLE_HIGH_HFP_VERSION},

    //Jeep Cherokee 2019, name:Uconnect
    {{{0x2c, 0xdc, 0xad, 0x04, 0, 0}}, 4, INTEROP_DISABLE_REJECT_NOTIFICATION},
    //Toyota RAV4, fc:62:b9:4f
    {{{0xfc, 0x62, 0xb9, 0x4f, 0, 0}}, 4, INTEROP_DISABLE_REJECT_NOTIFICATION},
    //do not send RejectNotification
    //BAOJUN RC-52021, 50:80:4a:d9
    {{{0x50, 0x80, 0x4a, 0xd9, 0, 0}}, 4, INTEROP_DISABLE_REJECT_NOTIFICATION},

    // Honda HFT Crider
    {{{0x30, 0xc3, 0xd9, 0, 0, 0}}, 3, INTEROP_RETRY_SDP_ON_ACL_DISCONNECT},
    // Honda HFT Oddyssey
    {{{0xbc, 0x75, 0x36, 0, 0, 0}}, 3, INTEROP_RETRY_SDP_ON_ACL_DISCONNECT},

    // Sabbat X12 Pro
    {{{0x00, 0x00, 0x00, 0x00, 0x14, 0x69}}, 6, INTEROP_STRICT_HFP_CALLSETUP_SPEC},
    {{{0x00, 0x00, 0x00, 0x00, 0x05, 0x9c}}, 6, INTEROP_STRICT_HFP_CALLSETUP_SPEC},

    //cadillac cue
    {{{0x30, 0xC3, 0xD9, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},

    //TODO: Check below peer addrs to verify.
    //{{{0x28, 0xA1, 0x83, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0xA0, 0x14, 0x3D, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x90, 0x03, 0xB7, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x00, 0x21, 0x3C, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x9C, 0xDF, 0x03, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0xE0, 0x75, 0x0A, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x48, 0xF0, 0x7B, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x04, 0x52, 0xC7, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},
    //{{{0x00, 0xE0, 0x4C, 0, 0, 0}}, 3, INTEROP_DISABLE_AAC_CODEC},

    //TODO: Check below peer addrs to verify.
    //{{{0x00, 0x08, 0x8a, 0, 0, 0}}, 3, INTEROP_DISABLE_CODEC_NEGOTIATION},

    {{{0x00, 0x15, 0x83, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    {{{0xBC, 0x30, 0x7E, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Volkswagen Passat 28:a1:83:94:17:df
    {{{0x28, 0xa1, 0x83, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Volkswagen LAVIDA (S5E8535-3818)
    {{{0x74, 0x95, 0xEC, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Volkswagen T-ROC (S5E8535-3815)
    {{{0x88, 0xBD, 0x78, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Hyundai Festa 2020 (S5E8535-3885)
    {{{0xA4, 0x04, 0x50, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // BMT X7 2019
    {{{0x2C, 0xDC, 0xAD, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // BT_PEUGEOT
    {{{0x00, 0x54, 0xAF, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Weichai U70 2020
    {{{0x11, 0x11, 0x22, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Porsche Cayenne 2019
    {{{0x44, 0x91, 0x60, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // BMW 4 Series 2019
    {{{0x00, 0x0a, 0x08, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // BMW M3 2018
    {{{0xA0, 0x56, 0xB2, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Geely New Vision 2020
    {{{0x22, 0x22, 0xA1, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    // Audi S6 2018
    {{{0x74, 0x6F, 0xF7, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    //BMW X4 2019 (S5E8835-3889)
    {{{0x28, 0x56, 0xC1, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    //LEXUS CT 2017 (S5E8835-3889)
    {{{0xA4, 0x08, 0xEA, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    //Porsche Carrera 911 2018 (S5E8835-3889)
    {{{0x48, 0xA9, 0xD2, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    //Volkswagen SAGITAR 2018 (S5E8835-3889)
    {{{0x30, 0xC3, 0xD9, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},
    //Volkswagen Tharu 2019 (S5E8835-3889)
    {{{0xB8, 0xD7, 0xAF, 0, 0, 0}}, 3, INTEROP_AVRCP_PLAY_STATUS_SYNC_WITH_AVDTP_STREAM},

    //VW BT 111
    {{{0x30, 0xC3, 0xD9, 0, 0, 0}}, 3, INTEROP_DISABLE_INBAND_RINGTONE},
};

typedef struct {
  char name[20];
  size_t length;
  interop_feature_t feature;
} interop_name_entry_t;

static const interop_name_entry_t interop_name_database[] = {
    // Carried over from auto_pair_devlist.conf migration
    {"Audi", 4, INTEROP_DISABLE_AUTO_PAIRING},
    {"BMW", 3, INTEROP_DISABLE_AUTO_PAIRING},
    {"Parrot", 6, INTEROP_DISABLE_AUTO_PAIRING},
    {"Car", 3, INTEROP_DISABLE_AUTO_PAIRING},

    // Nissan Quest rejects pairing after "0000"
    {"NISSAN", 6, INTEROP_DISABLE_AUTO_PAIRING},

    // Subaru car kits ("CAR M_MEDIA")
    {"CAR", 3, INTEROP_DISABLE_AUTO_PAIRING},

    // Pixel C Keyboard doesn't respond to service changed indications.
    {"Pixel C Keyboard", 16, INTEROP_GATTC_NO_SERVICE_CHANGED_IND},

    // Kenwood KMM-BT518HD - no audio when A2DP codec sample rate is changed
    {"KMM-BT51*HD", 11, INTEROP_DISABLE_AVDTP_RECONFIGURE},

    // Nintendo Switch Pro Controller and Joy Con - do not set sniff interval
    // dynamically. They require custom HID report command to change mode.
    {"Pro Controller", 14, INTEROP_HID_HOST_LIMIT_SNIFF_INTERVAL},
    {"Joy-Con", 7, INTEROP_HID_HOST_LIMIT_SNIFF_INTERVAL},
};
