
/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */
#include <hardware/bluetooth.h>

#include "bta/av/bta_av_int.h"
#include "stack/btm/security_device_record.h"

#define HCI_OFFLOAD_DATA_DUMP_MODE_DISABLE (0x00)
#define HCI_OFFLOAD_DATA_DUMP_MODE_AUDIOSS_TO_AIR (0X01) // Audio Subsystem
#define HCI_OFFLOAD_DATA_DUMP_MODE_L2CAP_TO_AIR (0X02)
#define HCI_OFFLOAD_DATA_DUMP_MODE_AUDIOSS_FROM_AIR (0X04)
#define HCI_OFFLOAD_DATA_DUMP_MODE_ISOAL_TO_AIR (0x08)
#define HCI_OFFLOAD_DATA_DUMP_MODE_ISOAL_FROM_AIR (0x10)

extern tBTM_SEC_DEV_REC* btm_find_dev(const RawAddress& bd_addr);

/*******************************************************************************
 *
 * Function         scsc_init_dbfw_a2dp_debug_info
 *
 * Description      Enable DBFW for A2DP
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_init_dbfw_a2dp_debug_info(tBTA_AV_SCB* p_scb);

/*******************************************************************************
 *
 * Function         scsc_cmd_dbfw
 *
 * Description      DBFW+ command handler
 *
 * Returns          bt_status_t
 *
 ******************************************************************************/
bt_status_t scsc_cmd_dbfw(uint16_t opcode, uint8_t subcode, uint16_t param1,
                          uint16_t param2);

/*******************************************************************************
 *
 * Function         scsc_enable_dbfw_nocrssi
 *
 * Description      Enables/Disables DBFW NOC RSSI
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_enable_dbfw_nocrssi(uint8_t enable);

/*******************************************************************************
 *
 * Function         scsc_set_l2cap_number_of_frames
 *
 * Description      Request Controller to enable game mode.
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_set_l2cap_number_of_frames(void);

/*******************************************************************************
 *
 * Function         scsc_init_tx_power_log
 *
 * Description      Enables TX Power logging.
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_init_tx_power_log(void);

/*******************************************************************************
 *
 * Function         scsc_enable_dbfw_lsto
 *
 * Description      Enable DBFW LSTO (OPCODE - 0xFDF1)
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_enable_dbfw_lsto(uint16_t handle, uint8_t enable);

/*******************************************************************************
 *
 * Function         scsc_offload_data_dump
 *
 * Description      offload data dump (OPCODE - 0xFDF1)
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_offload_data_dump(uint8_t mode, uint16_t duration);