/*
 * Copyright (©) 2020 Samsung Electronics Co., Ltd. All rights reserved.
 */

#define LOG_TAG "bt_scsc"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <hardware/bluetooth.h>

#include "bt_target.h"
#include "bt_types.h"
#include "btm_api.h"
#include "btm_int.h"
#include "hcidefs.h"
#include "osi/include/properties.h"
#include "scsc.h"

#define HCI_ENABLE_DBFW_NOCRSSI_SUB 0x40
#define ENABLE_DBFW_NOCRSSI_LEN 2

#define BLUETOOTH_TX_POWER_LOG_ENABLE "persist.bluetooth.tx_power_log.enable"
#define HCI_VSC_TX_POWER_LOG_LEN 0x01
#define HCI_VSE_TX_POWER_LOG_SUB_EVT_LEN 0x08
#define HCI_VSC_SET_L2CAP_SBC_FRAMES_LEN 1

#define ENABLE_DBFW_A2DP_DEBUG_INFO_LEN 6

#define HCI_ENABLE_DBFW_LSTO_SUB 0x10
#define HCI_ENABLE_SET_L2CAP_SBC_FRAMES 0x04

#define ENABLE_DBFW_LSTO_LEN 4
#define OFFLOAD_DATA_DUMP_LEN 3

#define SCSC_TRACE_EVENT(...)                                       \
  {                                                                 \
    LogMsg(TRACE_CTRL_GENERAL | TRACE_LAYER_NONE | TRACE_ORG_APPL | \
               TRACE_TYPE_EVENT,                                    \
           ##__VA_ARGS__);                                          \
  }

#define SCSC_TRACE_DEBUG(...)                                       \
  {                                                                 \
    LogMsg(TRACE_CTRL_GENERAL | TRACE_LAYER_NONE | TRACE_ORG_APPL | \
               TRACE_TYPE_DEBUG,                                    \
           ##__VA_ARGS__);                                          \
  }

#define SCSC_TRACE_WARNING(...)                                     \
  {                                                                 \
    LogMsg(TRACE_CTRL_GENERAL | TRACE_LAYER_NONE | TRACE_ORG_APPL | \
               TRACE_TYPE_WARNING,                                  \
           ##__VA_ARGS__);                                          \
  }

static void scsc_enable_dbfw_a2dp_debug_info_vse_cback(uint8_t len,
                                                       const uint8_t* p) {
  uint8_t event, sub_event = 0;
  STREAM_TO_UINT8(event, p);
  STREAM_TO_UINT8(sub_event, p);

  SCSC_TRACE_EVENT("%s: called with event:0x%x sub_event:0x%x", __func__, event,
                   sub_event);
  if (sub_event == HCI_VSE_DBFW_A2DP_DEBUG_INFO_SUB_EVT) {
    SCSC_TRACE_WARNING("DBFW+ A2DP debug info event recieved");
  }
  if (sub_event == HCI_VSE_DBFW_A2DP_DEBUG_CLASS_SUB_EVT) {
    SCSC_TRACE_WARNING("DBFW+ A2DP debug classification event recieved");
  }
}

static void scsc_enable_dbfw_a2dp_debug_info(uint8_t enable,
                                             uint16_t conn_handle,
                                             uint16_t delay_threshold) {
  uint8_t param[ENABLE_DBFW_A2DP_DEBUG_INFO_LEN], *p;

  SCSC_TRACE_DEBUG("%s conn_handle=0x%02x delay_threshold=%d", __FUNCTION__,
                   conn_handle, delay_threshold);

  p = param;
  memset(param, 0, ENABLE_DBFW_A2DP_DEBUG_INFO_LEN);

  UINT8_TO_STREAM(p, HCI_START_DBFW_A2DP_DEBUG_INFO_SUB);
  UINT8_TO_STREAM(p, enable);
  UINT16_TO_STREAM(p, conn_handle);
  UINT16_TO_STREAM(p, delay_threshold);

  BTM_VendorSpecificCommand(HCI_GRP_VENDOR_SPECIFIC_DBFW,
                            ENABLE_DBFW_A2DP_DEBUG_INFO_LEN, param, NULL);
}

void scsc_init_dbfw_a2dp_debug_info(tBTA_AV_SCB* p_scb) {
  char build_type[PROPERTY_VALUE_MAX] = {0};
  uint8_t enable;

  osi_property_get("ro.build.type", build_type, "");
  if (strcmp(build_type, "user") == 0) {
    SCSC_TRACE_DEBUG("%s is not supported in user mode", __FUNCTION__);
    return;
  }

  enable =
    osi_property_get_bool("persist.bluetooth.dbfw.a2dp_debug_enable", true);

  if (enable) {
    tBTM_SEC_DEV_REC* p_dev_rec = btm_find_dev(p_scb->PeerAddress());

    uint16_t delay_threshold = (uint16_t)osi_property_get_int32(
        "persist.bluetooth.dbfw.delay_threshold", 160);

    if (p_dev_rec != NULL) {
      scsc_enable_dbfw_a2dp_debug_info(1, p_dev_rec->hci_handle,
                                       delay_threshold);
      tBTM_STATUS retval = BTM_RegisterForVSEvents(
          scsc_enable_dbfw_a2dp_debug_info_vse_cback, true);
      if (retval != BTM_SUCCESS) {
        SCSC_TRACE_WARNING(
            "%s: Failed to register DBFW+ A2DP Debug Info VSEvents: %d",
            __func__, retval);
      }
    }
  }
}

static bt_status_t scsc_enable_dbfw_sco_dump(uint16_t opcode, uint8_t subcode,
                                             uint8_t mode, uint16_t duration) {
  uint8_t param[4], *p;

  SCSC_TRACE_DEBUG("%s", __FUNCTION__);

  p = param;

  memset(param, 0, 4);

  UINT8_TO_STREAM(p, subcode);
  UINT8_TO_STREAM(p, mode);
  UINT16_TO_STREAM(p, duration);

  BTM_VendorSpecificCommand(opcode, 4, param, NULL);

  return BT_STATUS_SUCCESS;
}

static bt_status_t scsc_enable_link_layer_log(uint16_t opcode, uint16_t mode) {
  uint8_t param[2], *p;

  SCSC_TRACE_DEBUG("%s", __FUNCTION__);

  p = param;
  memset(param, 0, 2);

  UINT16_TO_STREAM(p, mode);

  BTM_VendorSpecificCommand(opcode, 2, param, NULL);

  return BT_STATUS_SUCCESS;
}

/*******************************************************************************
 *
 * Function         scsc_cmd_dbfw
 *
 * Description      DBFW+ command handler
 *
 * Returns          bt_status_t
 *
 ******************************************************************************/
bt_status_t scsc_cmd_dbfw(uint16_t opcode, uint8_t subcode, uint16_t param1,
                          uint16_t param2) {
  bt_status_t status = BT_STATUS_FAIL;

  SCSC_TRACE_DEBUG("%s opcode=%d subcode=%d param1=%d param2=%", __FUNCTION__,
                   opcode, subcode, param1, param2);

  switch (opcode) {
    case HCI_GRP_VENDOR_SPECIFIC_DBFW:
      if (subcode == HCI_START_DBFW_SCO_DUMP_SUB)
        status = scsc_enable_dbfw_sco_dump(opcode, subcode, param1, param2);
      break;
    case HCI_GRP_VENDOR_SPECIFIC_LINK_LAYER:
      status = scsc_enable_link_layer_log(opcode, param1);
      break;
    default:
      status = BT_STATUS_UNSUPPORTED;
      break;
  }

  return status;
}

/*******************************************************************************
 *
 * Function         scsc_enable_dbfw_nocrssi
 *
 * Description      Enables/Disables DBFW NOC RSSI
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_enable_dbfw_nocrssi(uint8_t enable) {
  uint8_t param[ENABLE_DBFW_NOCRSSI_LEN], *p;
  uint8_t subcommand;

  SCSC_TRACE_DEBUG("%s", __FUNCTION__);

  p = param;
  memset(param, 0, ENABLE_DBFW_NOCRSSI_LEN);
  subcommand = HCI_ENABLE_DBFW_NOCRSSI_SUB;

  UINT8_TO_STREAM(p, subcommand);
  UINT8_TO_STREAM(p, enable);

  BTM_VendorSpecificCommand(HCI_GRP_VENDOR_SPECIFIC_DBFW,
                            ENABLE_DBFW_NOCRSSI_LEN, param, NULL);
}

/*******************************************************************************
 *
 * Function         scsc_btif_tx_power_log_vsc_send
 *
 * Description      Request Controller to Enable/Disable TX Power logging
 *
 * Returns          void
 *
 ******************************************************************************/
static void scsc_btif_tx_power_log_vsc_send(uint8_t enable) {
  uint8_t param[HCI_VSC_TX_POWER_LOG_LEN], *p;

  SCSC_TRACE_DEBUG("%s: enable = %d", __func__, enable);

  p = param;
  memset(param, 0, sizeof(param));

  UINT8_TO_STREAM(p, enable);

  BTM_VendorSpecificCommand(HCI_GRP_VENDOR_SPECIFIC_TX_POWER_LOG,
                            HCI_VSC_TX_POWER_LOG_LEN, param, NULL);
}

/*******************************************************************************
 *
 * Function         scsc_set_l2cap_number_of_frames
 *
 * Description      Request Controller to enable game mode
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_set_l2cap_number_of_frames() {
  uint8_t param[HCI_VSC_SET_L2CAP_SBC_FRAMES_LEN], *p;

  p = param;
  memset(param, 0, sizeof(param));

  UINT8_TO_STREAM(p, HCI_ENABLE_SET_L2CAP_SBC_FRAMES);

  SCSC_TRACE_DEBUG("%s: param = 0x%x", __func__, *param);

  BTM_VendorSpecificCommand(HCI_GRP_VENDOR_SPECIFIC_L2CAP_FRAMES,
                            HCI_VSC_SET_L2CAP_SBC_FRAMES_LEN, param, NULL);
}

/*******************************************************************************
 *
 * Function         scsc_tx_power_log_vse_cback
 *
 * Description      Callback invoked on receiving of Vendor Specific Events.
 *                  This function will call if Bluetooth TX Power logging
 *                  sub-event is identified.
 *
 * Parameters:      length - Lengths of all of the parameters contained in the
 *                    Vendor Specific Event.
 *                  p - A pointer to the TX power report which is sent
 *                    from the Bluetooth controller via Vendor Specific Event.
 *
 ******************************************************************************/
static void scsc_tx_power_log_vse_cback(uint8_t length, const uint8_t* p) {
  if (length == 0) {
    SCSC_TRACE_WARNING("%s: Lengths of all of the parameters are zero.",
                       __func__);
    return;
  }

  uint8_t sub_event = 0;
  STREAM_TO_UINT8(sub_event, p);

  if (sub_event == HCI_VSE_SUBCODE_TX_POWER_LOG_SUB_EVT) {
    length--;

    if (length != HCI_VSE_TX_POWER_LOG_SUB_EVT_LEN) {
      SCSC_TRACE_WARNING("%s: Parameters length invalid: %d", __func__, length);
      return;
    }
    uint16_t connection_handle;
    uint32_t piconet_clock;
    int8_t power_level;
    uint8_t rf_path;

    STREAM_TO_UINT16(connection_handle, p);
    STREAM_TO_UINT32(piconet_clock, p);
    STREAM_TO_INT8(power_level, p);
    STREAM_TO_UINT8(rf_path, p);

    SCSC_TRACE_EVENT(
        "%s: Received parameters are: Connection_handle: 0x%04X piconet_clock: "
        "0x%08X, rf_path: 0x%02X, power_level: %d",
        __func__, connection_handle, piconet_clock, rf_path, power_level);
  }
}

/*******************************************************************************
 *
 * Function         scsc_tx_power_log_vse_register
 *
 * Description      Register/Deregister for TX Power logging VSE sub
 *                  event Callback.
 *
 * Parameters:      is_register - True/False to register/unregister for VSE.
 *
 ******************************************************************************/
static void scsc_tx_power_log_vse_register(bool is_register) {
  tBTM_STATUS retval =
      BTM_RegisterForVSEvents(scsc_tx_power_log_vse_cback, is_register);
  if (retval != BTM_SUCCESS) {
    SCSC_TRACE_WARNING("%s: Failed to register Tx Power Log Info VSEvents: %d",
                       __func__, retval);
  }
}

/*******************************************************************************
 *
 * Function         scsc_init_tx_power_log
 *
 * Description      Enables TX Power logging.
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_init_tx_power_log() {
  bool enable = osi_property_get_bool(BLUETOOTH_TX_POWER_LOG_ENABLE, false);

  if (enable) {
    scsc_btif_tx_power_log_vsc_send(enable);
    scsc_tx_power_log_vse_register(enable);
  }
}

/*******************************************************************************
 *
 * Function         scsc_enable_dbfw_lsto
 *
 * Description      Enable DBFW LSTO (OPCODE - 0xFDF1)
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_enable_dbfw_lsto(uint16_t handle, uint8_t enable) {
  uint8_t param[ENABLE_DBFW_LSTO_LEN], *p;
  uint8_t subcommand;
  uint16_t connetion_handle;

  SCSC_TRACE_DEBUG("%s", __func__);

  p = param;
  memset(param, 0, ENABLE_DBFW_LSTO_LEN);
  subcommand = HCI_ENABLE_DBFW_LSTO_SUB;
  connetion_handle = handle;

  UINT8_TO_STREAM(p, subcommand);
  UINT8_TO_STREAM(p, enable);
  UINT16_TO_STREAM(p, connetion_handle);

  BTM_VendorSpecificCommand(HCI_GRP_VENDOR_SPECIFIC_DBFW, ENABLE_DBFW_LSTO_LEN,
                            param, NULL);
}

/*******************************************************************************
 *
 * Function         scsc_offload_data_dump
 *
 * Description      offload data dump (OPCODE - 0xFDF1)
 *
 * Returns          void
 *
 ******************************************************************************/
void scsc_offload_data_dump(uint8_t mode, uint16_t duration) {
  uint8_t param[OFFLOAD_DATA_DUMP_LEN], *p;

  SCSC_TRACE_DEBUG("%s mode: 0x%02x, duration: 0x%04x", __func__, mode,
    duration);

  p = param;
  memset(param, 0, OFFLOAD_DATA_DUMP_LEN);

  UINT8_TO_STREAM(p, mode);
  UINT16_TO_STREAM(p, duration);

  BTM_VendorSpecificCommand(HCI_VENDOR_SPECIFIC_OFFLOAD_DATA_DUMP,
                            OFFLOAD_DATA_DUMP_LEN, param, NULL);
}